// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "SurvivalGame.h"
#include "SMedKit.h"



ASMedKit::ASMedKit(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	StorageSlot = EInventorySlot::Med;

	GetWeaponMesh()->AddLocalRotation(FRotator(0, 0, -90));

	bIsActive = true;

	HealAmount = 75.0f;

	ChannelDuration = 0.0f;

	/* 1 use */
	StartAmmo = 1;
}


void ASMedKit::BeginPlay()
{
	Super::BeginPlay();

	/* Create an instance unique to this actor instance to manipulate emissive intensity */
	USkeletalMeshComponent* MeshComp = GetWeaponMesh();
	if (MeshComp)
	{
		//MatDynamic = MeshComp->CreateAndSetMaterialInstanceDynamic(0);
	}
}


void ASMedKit::HandleFiring()
{
	if (Role == ROLE_Authority)
	{
		/* Toggle light,cone and material when Fired */
		bIsActive = !bIsActive;


		HealUser();
	}
}

void ASMedKit::HealUser() {

	ASCharacter* User;
	User = GetPawnOwner();

	float CurrentHealth = User->GetHealth();
	float MaxHealth = User->GetMaxHealth();

	if (CurrentHealth != MaxHealth)
		User->RestoreCondition(HealAmount, 0.f);
}


void ASMedKit::OnEnterInventory(ASCharacter* NewOwner)
{
	if (Role == ROLE_Authority)
	{
		bIsActive = false;
	}

	Super::OnEnterInventory(NewOwner);
}



void ASMedKit::OnEquipFinished()
{
	Super::OnEquipFinished();

	if (Role == ROLE_Authority)
	{
		bIsActive = true;
	}
}


void ASMedKit::OnUnEquip()
{
	Super::OnUnEquip();

	if (Role == ROLE_Authority)
	{
		bIsActive = false;
	}
}


void ASMedKit::OnRep_IsActive()
{
	HealUser();
}


void ASMedKit::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASFlashlight, bIsActive);
}