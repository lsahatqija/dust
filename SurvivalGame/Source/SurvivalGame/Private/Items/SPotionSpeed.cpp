// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "SurvivalGame.h"
#include "SPotionSpeed.h"



ASPotionSpeed::ASPotionSpeed(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	StorageSlot = EInventorySlot::Potion;

	GetWeaponMesh()->AddLocalRotation(FRotator(0, 0, -90));

	bIsActive = true;

	HealAmount = 0.0f;

	ChannelDuration = 0.0f;

	PotionDuration = 20.0f;

	/* 1 use */
	StartAmmo = 1;
}


void ASPotionSpeed::BeginPlay()
{
	Super::BeginPlay();

	/* Create an instance unique to this actor instance to manipulate emissive intensity */
	USkeletalMeshComponent* MeshComp = GetWeaponMesh();
	if (MeshComp)
	{
		//MatDynamic = MeshComp->CreateAndSetMaterialInstanceDynamic(0);
	}
}


void ASPotionSpeed::HandleFiring()
{
	if (Role == ROLE_Authority)
	{
		/* Toggle light,cone and material when Fired */
		bIsActive = !bIsActive;


		ApplySpeed();
	}
}

void ASPotionSpeed::ApplySpeed() {
	ASCharacter* User;
	User = GetPawnOwner();

	//User->SetSpeedPotionUsed(true);
	User->SetSprinting(true);

	PotionDurationTimer(PotionDuration, User);
}

void ASPotionSpeed::PotionDurationTimer(float PotionDurationTime, ASCharacter* NewOwner) {
	//FTimerHandle PotionDurationHandle;

	FTimerDelegate TimerDel;
	TimerDel.BindUFunction(this, FName("UnApplySpeed"), NewOwner);

	GetWorldTimerManager().SetTimer(PotionDurationHandle, TimerDel, PotionDurationTime, false);
}

void ASPotionSpeed::UnApplySpeed(ASCharacter* User) {
	/*ASCharacter* User;
	User = GetPawnOwner();*/

	//User->SetSpeedPotionUsed(false);
	User->SetSprinting(false);

	GetWorldTimerManager().ClearTimer(PotionDurationHandle);
}


void ASPotionSpeed::OnEnterInventory(ASCharacter* NewOwner)
{
	if (Role == ROLE_Authority)
	{
		bIsActive = false;
	}

	Super::OnEnterInventory(NewOwner);
}



void ASPotionSpeed::OnEquipFinished()
{
	Super::OnEquipFinished();

	if (Role == ROLE_Authority)
	{
		bIsActive = true;
	}
}


void ASPotionSpeed::OnUnEquip()
{
	Super::OnUnEquip();

	if (Role == ROLE_Authority)
	{
		bIsActive = false;
	}
}


void ASPotionSpeed::OnRep_IsActive()
{
	ApplySpeed();
}


void ASPotionSpeed::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASPotionSpeed, bIsActive);
}