// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SWeapon.h"
#include "SPotionSpeed.generated.h"

/**
*
*/
UCLASS(ABSTRACT)
class SURVIVALGAME_API ASPotionSpeed : public ASWeapon
{
	GENERATED_BODY()

		ASPotionSpeed(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

public:

	UPROPERTY(EditDefaultsOnly)
		FName LightAttachPoint;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_IsActive)
		bool bIsActive;

	virtual void HandleFiring() override;

	virtual void OnEquipFinished() override;

	virtual void OnUnEquip() override;

	virtual void OnEnterInventory(ASCharacter* NewOwner);

	UFUNCTION()
	virtual void ApplySpeed();

	virtual void PotionDurationTimer(float PotionDurationTime, ASCharacter* NewOwner);

	UFUNCTION()
	virtual void UnApplySpeed(ASCharacter* NewOwner);

	UFUNCTION()
		void OnRep_IsActive();

	float HealAmount;

	float ChannelDuration;

	UPROPERTY(EditDefaultsOnly)
	float PotionDuration;

	FTimerHandle PotionDurationHandle;

};
