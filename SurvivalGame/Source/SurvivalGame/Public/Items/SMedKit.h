// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "SWeapon.h"
#include "SMedKit.generated.h"

/**
*
*/
UCLASS(ABSTRACT)
class SURVIVALGAME_API ASMedKit : public ASWeapon
{
	GENERATED_BODY()

		ASMedKit(const FObjectInitializer& ObjectInitializer);

	virtual void BeginPlay() override;

public:

	UPROPERTY(EditDefaultsOnly)
		FName LightAttachPoint;

	UPROPERTY(Transient, ReplicatedUsing = OnRep_IsActive)
		bool bIsActive;
	
	virtual void HandleFiring() override;

	virtual void OnEquipFinished() override;

	virtual void OnUnEquip() override;

	virtual void OnEnterInventory(ASCharacter* NewOwner);

	virtual void HealUser();

	UFUNCTION()
		void OnRep_IsActive();

	UPROPERTY(EditDefaultsOnly)
	float HealAmount;

	float ChannelDuration;


};
