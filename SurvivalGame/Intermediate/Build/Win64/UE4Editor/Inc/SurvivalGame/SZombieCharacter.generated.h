// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
class APawn;
struct FVector;
#ifdef SURVIVALGAME_SZombieCharacter_generated_h
#error "SZombieCharacter.generated.h already included, missing '#pragma once' in SZombieCharacter.h"
#endif
#define SURVIVALGAME_SZombieCharacter_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_RPC_WRAPPERS \
	virtual void BroadcastUpdateAudioLoop_Implementation(bool bNewSensedTarget); \
	virtual void SimulateMeleeStrike_Implementation(); \
 \
	DECLARE_FUNCTION(execBroadcastUpdateAudioLoop) \
	{ \
		P_GET_UBOOL(Z_Param_bNewSensedTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BroadcastUpdateAudioLoop_Implementation(Z_Param_bNewSensedTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSimulateMeleeStrike) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SimulateMeleeStrike_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPerformMeleeStrike) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_HitActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PerformMeleeStrike(Z_Param_HitActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnMeleeCompBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnMeleeCompBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHearNoise) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_PawnInstigator); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Volume); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHearNoise(Z_Param_PawnInstigator,Z_Param_Out_Location,Z_Param_Volume); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSeePlayer) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_Pawn); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSeePlayer(Z_Param_Pawn); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBroadcastUpdateAudioLoop) \
	{ \
		P_GET_UBOOL(Z_Param_bNewSensedTarget); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BroadcastUpdateAudioLoop_Implementation(Z_Param_bNewSensedTarget); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSimulateMeleeStrike) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SimulateMeleeStrike_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPerformMeleeStrike) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_HitActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PerformMeleeStrike(Z_Param_HitActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnMeleeCompBeginOverlap) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnMeleeCompBeginOverlap(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnHearNoise) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_PawnInstigator); \
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Volume); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHearNoise(Z_Param_PawnInstigator,Z_Param_Out_Location,Z_Param_Volume); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnSeePlayer) \
	{ \
		P_GET_OBJECT(APawn,Z_Param_Pawn); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnSeePlayer(Z_Param_Pawn); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_EVENT_PARMS \
	struct SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms \
	{ \
		bool bNewSensedTarget; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASZombieCharacter(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieCharacter(); \
public: \
	DECLARE_CLASS(ASZombieCharacter, ASBaseCharacter, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASZombieCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_INCLASS \
private: \
	static void StaticRegisterNativesASZombieCharacter(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieCharacter(); \
public: \
	DECLARE_CLASS(ASZombieCharacter, ASBaseCharacter, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASZombieCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASZombieCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASZombieCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASZombieCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASZombieCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASZombieCharacter(ASZombieCharacter&&); \
	NO_API ASZombieCharacter(const ASZombieCharacter&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASZombieCharacter(ASZombieCharacter&&); \
	NO_API ASZombieCharacter(const ASZombieCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASZombieCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASZombieCharacter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASZombieCharacter)


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SenseTimeOut() { return STRUCT_OFFSET(ASZombieCharacter, SenseTimeOut); } \
	FORCEINLINE static uint32 __PPO__PawnSensingComp() { return STRUCT_OFFSET(ASZombieCharacter, PawnSensingComp); } \
	FORCEINLINE static uint32 __PPO__MeleeCollisionComp() { return STRUCT_OFFSET(ASZombieCharacter, MeleeCollisionComp); } \
	FORCEINLINE static uint32 __PPO__PunchDamageType() { return STRUCT_OFFSET(ASZombieCharacter, PunchDamageType); } \
	FORCEINLINE static uint32 __PPO__MeleeDamage() { return STRUCT_OFFSET(ASZombieCharacter, MeleeDamage); } \
	FORCEINLINE static uint32 __PPO__MeleeAnimMontage() { return STRUCT_OFFSET(ASZombieCharacter, MeleeAnimMontage); } \
	FORCEINLINE static uint32 __PPO__SoundPlayerNoticed() { return STRUCT_OFFSET(ASZombieCharacter, SoundPlayerNoticed); } \
	FORCEINLINE static uint32 __PPO__SoundHunting() { return STRUCT_OFFSET(ASZombieCharacter, SoundHunting); } \
	FORCEINLINE static uint32 __PPO__SoundIdle() { return STRUCT_OFFSET(ASZombieCharacter, SoundIdle); } \
	FORCEINLINE static uint32 __PPO__SoundWandering() { return STRUCT_OFFSET(ASZombieCharacter, SoundWandering); } \
	FORCEINLINE static uint32 __PPO__SoundAttackMelee() { return STRUCT_OFFSET(ASZombieCharacter, SoundAttackMelee); } \
	FORCEINLINE static uint32 __PPO__AudioLoopComp() { return STRUCT_OFFSET(ASZombieCharacter, AudioLoopComp); }


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_8_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_AI_SZombieCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
