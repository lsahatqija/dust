// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SZombieAIController_generated_h
#error "SZombieAIController.generated.h already included, missing '#pragma once' in SZombieAIController.h"
#endif
#define SURVIVALGAME_SZombieAIController_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASZombieAIController(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieAIController(); \
public: \
	DECLARE_CLASS(ASZombieAIController, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASZombieAIController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASZombieAIController(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieAIController(); \
public: \
	DECLARE_CLASS(ASZombieAIController, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASZombieAIController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASZombieAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASZombieAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASZombieAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASZombieAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASZombieAIController(ASZombieAIController&&); \
	NO_API ASZombieAIController(const ASZombieAIController&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASZombieAIController(ASZombieAIController&&); \
	NO_API ASZombieAIController(const ASZombieAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASZombieAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASZombieAIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASZombieAIController)


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TargetEnemyKeyName() { return STRUCT_OFFSET(ASZombieAIController, TargetEnemyKeyName); } \
	FORCEINLINE static uint32 __PPO__PatrolLocationKeyName() { return STRUCT_OFFSET(ASZombieAIController, PatrolLocationKeyName); } \
	FORCEINLINE static uint32 __PPO__CurrentWaypointKeyName() { return STRUCT_OFFSET(ASZombieAIController, CurrentWaypointKeyName); } \
	FORCEINLINE static uint32 __PPO__BotTypeKeyName() { return STRUCT_OFFSET(ASZombieAIController, BotTypeKeyName); }


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_13_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_AI_SZombieAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
