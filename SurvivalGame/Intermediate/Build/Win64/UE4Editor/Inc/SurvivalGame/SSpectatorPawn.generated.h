// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SSpectatorPawn_generated_h
#error "SSpectatorPawn.generated.h already included, missing '#pragma once' in SSpectatorPawn.h"
#endif
#define SURVIVALGAME_SSpectatorPawn_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASSpectatorPawn(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASSpectatorPawn(); \
public: \
	DECLARE_CLASS(ASSpectatorPawn, ASpectatorPawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASSpectatorPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASSpectatorPawn(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASSpectatorPawn(); \
public: \
	DECLARE_CLASS(ASSpectatorPawn, ASpectatorPawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASSpectatorPawn) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASSpectatorPawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASSpectatorPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASSpectatorPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASSpectatorPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASSpectatorPawn(ASSpectatorPawn&&); \
	NO_API ASSpectatorPawn(const ASSpectatorPawn&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASSpectatorPawn(ASSpectatorPawn&&); \
	NO_API ASSpectatorPawn(const ASSpectatorPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASSpectatorPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASSpectatorPawn); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASSpectatorPawn)


#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_PRIVATE_PROPERTY_OFFSET
#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Player_SSpectatorPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
