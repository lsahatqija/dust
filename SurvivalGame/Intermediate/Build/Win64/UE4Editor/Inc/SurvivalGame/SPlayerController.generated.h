// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EHUDMessage : uint8;
enum class EHUDState : uint8;
#ifdef SURVIVALGAME_SPlayerController_generated_h
#error "SPlayerController.generated.h already included, missing '#pragma once' in SPlayerController.h"
#endif
#define SURVIVALGAME_SPlayerController_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_RPC_WRAPPERS \
	virtual void ClientHUDMessage_Implementation(EHUDMessage MessageID); \
	virtual void ClientHUDStateChanged_Implementation(EHUDState NewState); \
	virtual bool ServerSuicide_Validate(); \
	virtual void ServerSuicide_Implementation(); \
 \
	DECLARE_FUNCTION(execSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Suicide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientHUDMessage) \
	{ \
		P_GET_ENUM(EHUDMessage,Z_Param_MessageID); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClientHUDMessage_Implementation(EHUDMessage(Z_Param_MessageID)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientHUDStateChanged) \
	{ \
		P_GET_ENUM(EHUDState,Z_Param_NewState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClientHUDStateChanged_Implementation(EHUDState(Z_Param_NewState)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSuicide_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSuicide_Validate")); \
			return; \
		} \
		this->ServerSuicide_Implementation(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Suicide(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientHUDMessage) \
	{ \
		P_GET_ENUM(EHUDMessage,Z_Param_MessageID); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClientHUDMessage_Implementation(EHUDMessage(Z_Param_MessageID)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execClientHUDStateChanged) \
	{ \
		P_GET_ENUM(EHUDState,Z_Param_NewState); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->ClientHUDStateChanged_Implementation(EHUDState(Z_Param_NewState)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSuicide) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSuicide_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSuicide_Validate")); \
			return; \
		} \
		this->ServerSuicide_Implementation(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_EVENT_PARMS \
	struct SPlayerController_eventClientHUDMessage_Parms \
	{ \
		EHUDMessage MessageID; \
	}; \
	struct SPlayerController_eventClientHUDStateChanged_Parms \
	{ \
		EHUDState NewState; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASPlayerController(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerController(); \
public: \
	DECLARE_CLASS(ASPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASPlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_INCLASS \
private: \
	static void StaticRegisterNativesASPlayerController(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerController(); \
public: \
	DECLARE_CLASS(ASPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASPlayerController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASPlayerController(ASPlayerController&&); \
	NO_API ASPlayerController(const ASPlayerController&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASPlayerController(ASPlayerController&&); \
	NO_API ASPlayerController(const ASPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASPlayerController)


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bRespawnImmediately() { return STRUCT_OFFSET(ASPlayerController, bRespawnImmediately); }


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_29_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerController_h


#define FOREACH_ENUM_EHUDMESSAGE(op) \
	op(EHUDMessage::Weapon_SlotTaken) \
	op(EHUDMessage::Character_EnergyRestored) \
	op(EHUDMessage::Game_SurviveStart) \
	op(EHUDMessage::Game_SurviveEnded) \
	op(EHUDMessage::None) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
