// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef SURVIVALGAME_SCarryObjectComponent_generated_h
#error "SCarryObjectComponent.generated.h already included, missing '#pragma once' in SCarryObjectComponent.h"
#endif
#define SURVIVALGAME_SCarryObjectComponent_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_RPC_WRAPPERS \
	virtual void OnRotateMulticast_Implementation(float DirectionYaw, float DirectionRoll); \
	virtual bool ServerRotate_Validate(float , float ); \
	virtual void ServerRotate_Implementation(float DirectionYaw, float DirectionRoll); \
	virtual bool ServerThrow_Validate(); \
	virtual void ServerThrow_Implementation(); \
	virtual void OnDropMulticast_Implementation(); \
	virtual bool ServerDrop_Validate(); \
	virtual void ServerDrop_Implementation(); \
	virtual void OnPickupMulticast_Implementation(AActor* FocusActor); \
	virtual bool ServerPickup_Validate(); \
	virtual void ServerPickup_Implementation(); \
 \
	DECLARE_FUNCTION(execOnRotateMulticast) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionYaw); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionRoll); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRotateMulticast_Implementation(Z_Param_DirectionYaw,Z_Param_DirectionRoll); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerRotate) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionYaw); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionRoll); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerRotate_Validate(Z_Param_DirectionYaw,Z_Param_DirectionRoll)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerRotate_Validate")); \
			return; \
		} \
		this->ServerRotate_Implementation(Z_Param_DirectionYaw,Z_Param_DirectionRoll); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerThrow) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerThrow_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerThrow_Validate")); \
			return; \
		} \
		this->ServerThrow_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnDropMulticast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnDropMulticast_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerDrop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerDrop_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerDrop_Validate")); \
			return; \
		} \
		this->ServerDrop_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPickupMulticast) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_FocusActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPickupMulticast_Implementation(Z_Param_FocusActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerPickup) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerPickup_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerPickup_Validate")); \
			return; \
		} \
		this->ServerPickup_Implementation(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRotateMulticast) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionYaw); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionRoll); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRotateMulticast_Implementation(Z_Param_DirectionYaw,Z_Param_DirectionRoll); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerRotate) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionYaw); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DirectionRoll); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerRotate_Validate(Z_Param_DirectionYaw,Z_Param_DirectionRoll)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerRotate_Validate")); \
			return; \
		} \
		this->ServerRotate_Implementation(Z_Param_DirectionYaw,Z_Param_DirectionRoll); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerThrow) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerThrow_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerThrow_Validate")); \
			return; \
		} \
		this->ServerThrow_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnDropMulticast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnDropMulticast_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerDrop) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerDrop_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerDrop_Validate")); \
			return; \
		} \
		this->ServerDrop_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPickupMulticast) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_FocusActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnPickupMulticast_Implementation(Z_Param_FocusActor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerPickup) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerPickup_Validate()) \
		{ \
			RPC_ValidateFailed(TEXT("ServerPickup_Validate")); \
			return; \
		} \
		this->ServerPickup_Implementation(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_EVENT_PARMS \
	struct SCarryObjectComponent_eventOnPickupMulticast_Parms \
	{ \
		AActor* FocusActor; \
	}; \
	struct SCarryObjectComponent_eventOnRotateMulticast_Parms \
	{ \
		float DirectionYaw; \
		float DirectionRoll; \
	}; \
	struct SCarryObjectComponent_eventServerRotate_Parms \
	{ \
		float DirectionYaw; \
		float DirectionRoll; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSCarryObjectComponent(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_USCarryObjectComponent(); \
public: \
	DECLARE_CLASS(USCarryObjectComponent, USpringArmComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(USCarryObjectComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSCarryObjectComponent(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_USCarryObjectComponent(); \
public: \
	DECLARE_CLASS(USCarryObjectComponent, USpringArmComponent, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(USCarryObjectComponent) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USCarryObjectComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USCarryObjectComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USCarryObjectComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USCarryObjectComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USCarryObjectComponent(USCarryObjectComponent&&); \
	NO_API USCarryObjectComponent(const USCarryObjectComponent&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USCarryObjectComponent(USCarryObjectComponent&&); \
	NO_API USCarryObjectComponent(const USCarryObjectComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USCarryObjectComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USCarryObjectComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USCarryObjectComponent)


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RotateSpeed() { return STRUCT_OFFSET(USCarryObjectComponent, RotateSpeed); }


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_11_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Components_SCarryObjectComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
