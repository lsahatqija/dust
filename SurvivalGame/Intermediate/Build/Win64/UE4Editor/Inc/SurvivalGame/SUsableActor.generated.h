// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SUsableActor_generated_h
#error "SUsableActor.generated.h already included, missing '#pragma once' in SUsableActor.h"
#endif
#define SURVIVALGAME_SUsableActor_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASUsableActor(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASUsableActor(); \
public: \
	DECLARE_CLASS(ASUsableActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASUsableActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_INCLASS \
private: \
	static void StaticRegisterNativesASUsableActor(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASUsableActor(); \
public: \
	DECLARE_CLASS(ASUsableActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASUsableActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASUsableActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASUsableActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASUsableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASUsableActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASUsableActor(ASUsableActor&&); \
	NO_API ASUsableActor(const ASUsableActor&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASUsableActor(ASUsableActor&&); \
	NO_API ASUsableActor(const ASUsableActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASUsableActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASUsableActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASUsableActor)


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComp() { return STRUCT_OFFSET(ASUsableActor, MeshComp); }


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_8_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Items_SUsableActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
