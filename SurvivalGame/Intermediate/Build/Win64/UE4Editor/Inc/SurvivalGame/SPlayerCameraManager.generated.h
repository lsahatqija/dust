// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SPlayerCameraManager_generated_h
#error "SPlayerCameraManager.generated.h already included, missing '#pragma once' in SPlayerCameraManager.h"
#endif
#define SURVIVALGAME_SPlayerCameraManager_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASPlayerCameraManager(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerCameraManager(); \
public: \
	DECLARE_CLASS(ASPlayerCameraManager, APlayerCameraManager, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASPlayerCameraManager) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASPlayerCameraManager(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerCameraManager(); \
public: \
	DECLARE_CLASS(ASPlayerCameraManager, APlayerCameraManager, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASPlayerCameraManager) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASPlayerCameraManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASPlayerCameraManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASPlayerCameraManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASPlayerCameraManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASPlayerCameraManager(ASPlayerCameraManager&&); \
	NO_API ASPlayerCameraManager(const ASPlayerCameraManager&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASPlayerCameraManager(ASPlayerCameraManager&&); \
	NO_API ASPlayerCameraManager(const ASPlayerCameraManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASPlayerCameraManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASPlayerCameraManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASPlayerCameraManager)


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_PRIVATE_PROPERTY_OFFSET
#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Player_SPlayerCameraManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
