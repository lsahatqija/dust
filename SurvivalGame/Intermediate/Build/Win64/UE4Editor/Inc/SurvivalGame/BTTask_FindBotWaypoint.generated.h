// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_BTTask_FindBotWaypoint_generated_h
#error "BTTask_FindBotWaypoint.generated.h already included, missing '#pragma once' in BTTask_FindBotWaypoint.h"
#endif
#define SURVIVALGAME_BTTask_FindBotWaypoint_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBTTask_FindBotWaypoint(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint(); \
public: \
	DECLARE_CLASS(UBTTask_FindBotWaypoint, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindBotWaypoint) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUBTTask_FindBotWaypoint(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint(); \
public: \
	DECLARE_CLASS(UBTTask_FindBotWaypoint, UBTTask_BlackboardBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(UBTTask_FindBotWaypoint) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindBotWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindBotWaypoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindBotWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindBotWaypoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindBotWaypoint(UBTTask_FindBotWaypoint&&); \
	NO_API UBTTask_FindBotWaypoint(const UBTTask_FindBotWaypoint&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBTTask_FindBotWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBTTask_FindBotWaypoint(UBTTask_FindBotWaypoint&&); \
	NO_API UBTTask_FindBotWaypoint(const UBTTask_FindBotWaypoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBTTask_FindBotWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBTTask_FindBotWaypoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBTTask_FindBotWaypoint)


#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET
#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_AI_BTTask_FindBotWaypoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
