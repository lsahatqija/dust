// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector_NetQuantizeNormal;
struct FHitResult;
#ifdef SURVIVALGAME_SWeaponInstant_generated_h
#error "SWeaponInstant.generated.h already included, missing '#pragma once' in SWeaponInstant.h"
#endif
#define SURVIVALGAME_SWeaponInstant_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_RPC_WRAPPERS \
	virtual bool ServerNotifyMiss_Validate(FVector_NetQuantizeNormal ); \
	virtual void ServerNotifyMiss_Implementation(FVector_NetQuantizeNormal ShootDir); \
	virtual bool ServerNotifyHit_Validate(const FHitResult , FVector_NetQuantizeNormal ); \
	virtual void ServerNotifyHit_Implementation(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir); \
 \
	DECLARE_FUNCTION(execOnRep_HitLocation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_HitLocation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerNotifyMiss) \
	{ \
		P_GET_STRUCT(FVector_NetQuantizeNormal,Z_Param_ShootDir); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerNotifyMiss_Validate(Z_Param_ShootDir)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerNotifyMiss_Validate")); \
			return; \
		} \
		this->ServerNotifyMiss_Implementation(Z_Param_ShootDir); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerNotifyHit) \
	{ \
		P_GET_STRUCT(FHitResult,Z_Param_Impact); \
		P_GET_STRUCT(FVector_NetQuantizeNormal,Z_Param_ShootDir); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerNotifyHit_Validate(Z_Param_Impact,Z_Param_ShootDir)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerNotifyHit_Validate")); \
			return; \
		} \
		this->ServerNotifyHit_Implementation(Z_Param_Impact,Z_Param_ShootDir); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_HitLocation) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_HitLocation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerNotifyMiss) \
	{ \
		P_GET_STRUCT(FVector_NetQuantizeNormal,Z_Param_ShootDir); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerNotifyMiss_Validate(Z_Param_ShootDir)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerNotifyMiss_Validate")); \
			return; \
		} \
		this->ServerNotifyMiss_Implementation(Z_Param_ShootDir); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerNotifyHit) \
	{ \
		P_GET_STRUCT(FHitResult,Z_Param_Impact); \
		P_GET_STRUCT(FVector_NetQuantizeNormal,Z_Param_ShootDir); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerNotifyHit_Validate(Z_Param_Impact,Z_Param_ShootDir)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerNotifyHit_Validate")); \
			return; \
		} \
		this->ServerNotifyHit_Implementation(Z_Param_Impact,Z_Param_ShootDir); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_EVENT_PARMS \
	struct SWeaponInstant_eventServerNotifyHit_Parms \
	{ \
		FHitResult Impact; \
		FVector_NetQuantizeNormal ShootDir; \
	}; \
	struct SWeaponInstant_eventServerNotifyMiss_Parms \
	{ \
		FVector_NetQuantizeNormal ShootDir; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASWeaponInstant(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponInstant(); \
public: \
	DECLARE_CLASS(ASWeaponInstant, ASWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASWeaponInstant) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASWeaponInstant(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponInstant(); \
public: \
	DECLARE_CLASS(ASWeaponInstant, ASWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASWeaponInstant) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASWeaponInstant(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASWeaponInstant) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeaponInstant); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeaponInstant); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeaponInstant(ASWeaponInstant&&); \
	NO_API ASWeaponInstant(const ASWeaponInstant&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASWeaponInstant(ASWeaponInstant&&); \
	NO_API ASWeaponInstant(const ASWeaponInstant&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASWeaponInstant); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASWeaponInstant); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASWeaponInstant)


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImpactTemplate() { return STRUCT_OFFSET(ASWeaponInstant, ImpactTemplate); } \
	FORCEINLINE static uint32 __PPO__TrailTargetParam() { return STRUCT_OFFSET(ASWeaponInstant, TrailTargetParam); } \
	FORCEINLINE static uint32 __PPO__TrailFX() { return STRUCT_OFFSET(ASWeaponInstant, TrailFX); } \
	FORCEINLINE static uint32 __PPO__TracerFX() { return STRUCT_OFFSET(ASWeaponInstant, TracerFX); } \
	FORCEINLINE static uint32 __PPO__MinimumProjectileSpawnDistance() { return STRUCT_OFFSET(ASWeaponInstant, MinimumProjectileSpawnDistance); } \
	FORCEINLINE static uint32 __PPO__TracerRoundInterval() { return STRUCT_OFFSET(ASWeaponInstant, TracerRoundInterval); } \
	FORCEINLINE static uint32 __PPO__HitImpactNotify() { return STRUCT_OFFSET(ASWeaponInstant, HitImpactNotify); } \
	FORCEINLINE static uint32 __PPO__HitDamage() { return STRUCT_OFFSET(ASWeaponInstant, HitDamage); } \
	FORCEINLINE static uint32 __PPO__DamageType() { return STRUCT_OFFSET(ASWeaponInstant, DamageType); } \
	FORCEINLINE static uint32 __PPO__WeaponRange() { return STRUCT_OFFSET(ASWeaponInstant, WeaponRange); } \
	FORCEINLINE static uint32 __PPO__AllowedViewDotHitDir() { return STRUCT_OFFSET(ASWeaponInstant, AllowedViewDotHitDir); } \
	FORCEINLINE static uint32 __PPO__ClientSideHitLeeway() { return STRUCT_OFFSET(ASWeaponInstant, ClientSideHitLeeway); }


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_11_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Items_SWeaponInstant_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
