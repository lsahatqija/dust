// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EHUDMessage : uint8;
#ifdef SURVIVALGAME_SGameState_generated_h
#error "SGameState.generated.h already included, missing '#pragma once' in SGameState.h"
#endif
#define SURVIVALGAME_SGameState_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_RPC_WRAPPERS \
	virtual void BroadcastGameMessage_Implementation(EHUDMessage NewMessage); \
 \
	DECLARE_FUNCTION(execBroadcastGameMessage) \
	{ \
		P_GET_ENUM(EHUDMessage,Z_Param_NewMessage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BroadcastGameMessage_Implementation(EHUDMessage(Z_Param_NewMessage)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetTimeOfDay) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_NewTimeOfDay); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetTimeOfDay(Z_Param_NewTimeOfDay); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRealSecondsTillSunrise) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetRealSecondsTillSunrise(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElapsedFullDaysInMinutes) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetElapsedFullDaysInMinutes(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElapsedDays) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetElapsedDays(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTotalScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetTotalScore(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBroadcastGameMessage) \
	{ \
		P_GET_ENUM(EHUDMessage,Z_Param_NewMessage); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->BroadcastGameMessage_Implementation(EHUDMessage(Z_Param_NewMessage)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetTimeOfDay) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_NewTimeOfDay); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetTimeOfDay(Z_Param_NewTimeOfDay); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRealSecondsTillSunrise) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetRealSecondsTillSunrise(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElapsedFullDaysInMinutes) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetElapsedFullDaysInMinutes(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetElapsedDays) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetElapsedDays(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetTotalScore) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=this->GetTotalScore(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_EVENT_PARMS \
	struct SGameState_eventBroadcastGameMessage_Parms \
	{ \
		EHUDMessage NewMessage; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASGameState(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameState(); \
public: \
	DECLARE_CLASS(ASGameState, AGameState, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASGameState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASGameState(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameState(); \
public: \
	DECLARE_CLASS(ASGameState, AGameState, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASGameState) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASGameState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASGameState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASGameState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASGameState(ASGameState&&); \
	NO_API ASGameState(const ASGameState&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASGameState(ASGameState&&); \
	NO_API ASGameState(const ASGameState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASGameState); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASGameState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASGameState)


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TotalScore() { return STRUCT_OFFSET(ASGameState, TotalScore); }


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_12_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_World_SGameState_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
