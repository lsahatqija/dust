// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SFlashlight_generated_h
#error "SFlashlight.generated.h already included, missing '#pragma once' in SFlashlight.h"
#endif
#define SURVIVALGAME_SFlashlight_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRep_IsActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_IsActive(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_IsActive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_IsActive(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASFlashlight(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASFlashlight(); \
public: \
	DECLARE_CLASS(ASFlashlight, ASWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASFlashlight) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASFlashlight(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASFlashlight(); \
public: \
	DECLARE_CLASS(ASFlashlight, ASWeapon, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASFlashlight) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASFlashlight(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASFlashlight) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASFlashlight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASFlashlight); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASFlashlight(ASFlashlight&&); \
	NO_API ASFlashlight(const ASFlashlight&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASFlashlight(ASFlashlight&&); \
	NO_API ASFlashlight(const ASFlashlight&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASFlashlight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASFlashlight); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASFlashlight)


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EmissiveParamName() { return STRUCT_OFFSET(ASFlashlight, EmissiveParamName); } \
	FORCEINLINE static uint32 __PPO__MaxEmissiveIntensity() { return STRUCT_OFFSET(ASFlashlight, MaxEmissiveIntensity); }


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Items_SFlashlight_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
