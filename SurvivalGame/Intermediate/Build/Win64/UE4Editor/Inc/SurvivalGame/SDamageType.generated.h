// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SDamageType_generated_h
#error "SDamageType.generated.h already included, missing '#pragma once' in SDamageType.h"
#endif
#define SURVIVALGAME_SDamageType_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSDamageType(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_USDamageType(); \
public: \
	DECLARE_CLASS(USDamageType, UDamageType, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(USDamageType) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSDamageType(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_USDamageType(); \
public: \
	DECLARE_CLASS(USDamageType, UDamageType, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(USDamageType) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USDamageType(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USDamageType) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USDamageType); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USDamageType); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USDamageType(USDamageType&&); \
	NO_API USDamageType(const USDamageType&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USDamageType(USDamageType&&); \
	NO_API USDamageType(const USDamageType&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USDamageType); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USDamageType); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USDamageType)


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bCanDieFrom() { return STRUCT_OFFSET(USDamageType, bCanDieFrom); } \
	FORCEINLINE static uint32 __PPO__HeadDmgModifier() { return STRUCT_OFFSET(USDamageType, HeadDmgModifier); } \
	FORCEINLINE static uint32 __PPO__LimbDmgModifier() { return STRUCT_OFFSET(USDamageType, LimbDmgModifier); }


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Items_SDamageType_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
