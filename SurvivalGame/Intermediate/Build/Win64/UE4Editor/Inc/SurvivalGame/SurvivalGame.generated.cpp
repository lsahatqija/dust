// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "SurvivalGame.h"
#include "SurvivalGame.generated.dep.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode1SurvivalGame() {}
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	AIMODULE_API class UClass* Z_Construct_UClass_UBTTask_BlackboardBase();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FRadialDamageEvent();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FPointDamageEvent();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FDamageEvent();
	ENGINE_API class UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API class UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API class UClass* Z_Construct_UClass_ACharacter();
	ENGINE_API class UClass* Z_Construct_UClass_USoundCue_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UDamageType_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API class UClass* Z_Construct_UClass_APawn_NoRegister();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API class UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	AIMODULE_API class UClass* Z_Construct_UClass_UBehaviorTree_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UAudioComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCapsuleComponent_NoRegister();
	AIMODULE_API class UClass* Z_Construct_UClass_UPawnSensingComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_ATargetPoint();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent();
	ENGINE_API class UClass* Z_Construct_UClass_UCharacterMovementComponent();
	ENGINE_API class UClass* Z_Construct_UClass_UDamageType();
	ENGINE_API class UClass* Z_Construct_UClass_AInfo();
	ENGINE_API class UClass* Z_Construct_UClass_AGameMode();
	ENGINE_API class UClass* Z_Construct_UClass_ADirectionalLight_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AHUD();
	ENGINE_API class UClass* Z_Construct_UClass_APlayerController();
	ENGINE_API class UClass* Z_Construct_UClass_AGameState();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UParticleSystem_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_ULocalPlayer();
	ENGINE_API class UClass* Z_Construct_UClass_USoundNode();
	ENGINE_API class UClass* Z_Construct_UClass_APlayerCameraManager();
	ENGINE_API class UClass* Z_Construct_UClass_APlayerStart();
	ENGINE_API class UClass* Z_Construct_UClass_APlayerState();
	ENGINE_API class UClass* Z_Construct_UClass_ASpectatorPawn();
	ENGINE_API class UClass* Z_Construct_UClass_ASkyLight_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UParticleSystemComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USpotLightComponent_NoRegister();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FVector_NetQuantizeNormal();
	AIMODULE_API class UClass* Z_Construct_UClass_AAIController();

	SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindPatrolLocation_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_UBTTask_FindPatrolLocation();
	SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EBotBehaviorType();
	SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EInventorySlot();
	SURVIVALGAME_API class UScriptStruct* Z_Construct_UScriptStruct_FTakeHitInfo();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_GetAimOffsets();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_GetHealth();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_GetMaxHealth();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_IsAlive();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_IsSprinting();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_IsTargeting();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_OnRep_LastTakeHitInfo();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_ServerSetSprinting();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBaseCharacter_ServerSetTargeting();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBaseCharacter_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBaseCharacter();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_GetCurrentWeapon();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_GetHunger();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_GetLastMakeNoiseTime();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_GetLastNoiseLoudness();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_GetMaxHunger();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_IsFiring();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_IsInitiatedJump();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_MakePawnNoise();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_OnRep_CurrentWeapon();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_RestoreCondition();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_ServerDropWeapon();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_ServerEquipWeapon();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_ServerSetIsJumping();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_ServerUse();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASCharacter_SwapToNewWeaponMesh();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASCharacter_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASCharacter();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_BroadcastUpdateAudioLoop();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_OnHearNoise();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_OnMeleeCompBeginOverlap();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_OnSeePlayer();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_PerformMeleeStrike();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASZombieCharacter_SimulateMeleeStrike();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieCharacter_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieCharacter();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBotWaypoint_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBotWaypoint();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnDropMulticast();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnPickupMulticast();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnRotateMulticast();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerDrop();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerPickup();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerRotate();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerThrow();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USCarryObjectComponent_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USCarryObjectComponent();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USCharacterMovementComponent_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USCharacterMovementComponent();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USDamageType_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USDamageType();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASMutator_CheckRelevance();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASMutator_InitGame();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASMutator_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASMutator();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameMode_CheckRelevance();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameMode_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameMode();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASCoopGameMode_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASCoopGameMode();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASOpenWorldGameMode_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASOpenWorldGameMode();
	SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EHUDState();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASHUD_GetCurrentState();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASHUD_MessageReceived();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASHUD_OnStateChanged();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASHUD_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASHUD();
	SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EHUDMessage();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerController_ClientHUDMessage();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerController_ClientHUDStateChanged();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerController_ServerSuicide();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerController_Suicide();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerController_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerController();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_BroadcastGameMessage();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_GetElapsedDays();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_GetElapsedFullDaysInMinutes();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_GetRealSecondsTillSunrise();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_GetTotalScore();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASGameState_SetTimeOfDay();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameState_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameState();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASImpactEffect_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASImpactEffect();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USLocalPlayer_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USLocalPlayer();
	SURVIVALGAME_API class UScriptStruct* Z_Construct_UScriptStruct_FReplacementInfo();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASMutator_WeaponReplacement_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASMutator_WeaponReplacement();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USoundNodeLocalPlayer_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_USoundNodeLocalPlayer();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerCameraManager_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerCameraManager();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerStart_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerStart();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerState_GetDeaths();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerState_GetKills();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerState_GetScore();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPlayerState_GetTeamNumber();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerState_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPlayerState();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASSpectatorPawn_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASSpectatorPawn();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASTimeOfDayManager_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASTimeOfDayManager();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASUsableActor_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASUsableActor();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBombActor_SimulateExplosion();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASBombActor_SimulateFuzeFX();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBombActor_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBombActor();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASPickupActor_OnRep_IsActive();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPickupActor_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASPickupActor();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASConsumableActor_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASConsumableActor();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponPickup_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponPickup();
	SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EWeaponState();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ClientStartReload();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetCurrentAmmo();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetCurrentAmmoInClip();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetMaxAmmo();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetMaxAmmoPerClip();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetPawnOwner();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_GetWeaponMesh();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_OnRep_BurstCounter();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_OnRep_MyPawn();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_OnRep_Reload();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ServerHandleFiring();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ServerStartFire();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ServerStartReload();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ServerStopFire();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeapon_ServerStopReload();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeapon_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeapon();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASFlashlight_OnRep_IsActive();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASFlashlight_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASFlashlight();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeaponInstant_OnRep_HitLocation();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeaponInstant_ServerNotifyHit();
	SURVIVALGAME_API class UFunction* Z_Construct_UFunction_ASWeaponInstant_ServerNotifyMiss();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponInstant_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASWeaponInstant();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieAIController_NoRegister();
	SURVIVALGAME_API class UClass* Z_Construct_UClass_ASZombieAIController();
	SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	void UBTTask_FindBotWaypoint::StaticRegisterNativesUBTTask_FindBotWaypoint()
	{
	}
	UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint_NoRegister()
	{
		return UBTTask_FindBotWaypoint::StaticClass();
	}
	UClass* Z_Construct_UClass_UBTTask_FindBotWaypoint()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBTTask_BlackboardBase();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = UBTTask_FindBotWaypoint::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20100080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UBTTask_FindBotWaypoint> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AI/BTTask_FindBotWaypoint.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AI/BTTask_FindBotWaypoint.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTTask_FindBotWaypoint, 2012890347);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTTask_FindBotWaypoint(Z_Construct_UClass_UBTTask_FindBotWaypoint, &UBTTask_FindBotWaypoint::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("UBTTask_FindBotWaypoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTask_FindBotWaypoint);
	void UBTTask_FindPatrolLocation::StaticRegisterNativesUBTTask_FindPatrolLocation()
	{
	}
	UClass* Z_Construct_UClass_UBTTask_FindPatrolLocation_NoRegister()
	{
		return UBTTask_FindPatrolLocation::StaticClass();
	}
	UClass* Z_Construct_UClass_UBTTask_FindPatrolLocation()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UBTTask_BlackboardBase();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = UBTTask_FindPatrolLocation::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20100080;


				static TCppClassTypeInfo<TCppClassTypeTraits<UBTTask_FindPatrolLocation> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AI/BTTask_FindPatrolLocation.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AI/BTTask_FindPatrolLocation.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Blackboard Task - Finds a position to a nearby waypoint"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBTTask_FindPatrolLocation, 3780999582);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBTTask_FindPatrolLocation(Z_Construct_UClass_UBTTask_FindPatrolLocation, &UBTTask_FindPatrolLocation::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("UBTTask_FindPatrolLocation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBTTask_FindPatrolLocation);
static UEnum* EBotBehaviorType_StaticEnum()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EBotBehaviorType();
		Singleton = GetStaticEnum(Z_Construct_UEnum_SurvivalGame_EBotBehaviorType, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("EBotBehaviorType"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBotBehaviorType(EBotBehaviorType_StaticEnum, TEXT("/Script/SurvivalGame"), TEXT("EBotBehaviorType"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_SurvivalGame_EBotBehaviorType()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UEnum_SurvivalGame_EBotBehaviorType_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBotBehaviorType"), 0, Get_Z_Construct_UEnum_SurvivalGame_EBotBehaviorType_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EBotBehaviorType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EBotBehaviorType::Passive"), 0);
			EnumNames.Emplace(TEXT("EBotBehaviorType::Patrolling"), 1);
			EnumNames.Emplace(TEXT("EBotBehaviorType::EBotBehaviorType_MAX"), 2);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EBotBehaviorType");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(ReturnEnum, TEXT("Passive.ToolTip"), TEXT("Does not move, remains in place until a player is spotted"));
			MetaData->SetValue(ReturnEnum, TEXT("Patrolling.ToolTip"), TEXT("Patrols a region until a player is spotted"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_SurvivalGame_EBotBehaviorType_CRC() { return 1439959001U; }
static UEnum* EInventorySlot_StaticEnum()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EInventorySlot();
		Singleton = GetStaticEnum(Z_Construct_UEnum_SurvivalGame_EInventorySlot, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("EInventorySlot"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EInventorySlot(EInventorySlot_StaticEnum, TEXT("/Script/SurvivalGame"), TEXT("EInventorySlot"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_SurvivalGame_EInventorySlot()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UEnum_SurvivalGame_EInventorySlot_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EInventorySlot"), 0, Get_Z_Construct_UEnum_SurvivalGame_EInventorySlot_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EInventorySlot"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EInventorySlot::Hands"), 0);
			EnumNames.Emplace(TEXT("EInventorySlot::Primary"), 1);
			EnumNames.Emplace(TEXT("EInventorySlot::Secondary"), 2);
			EnumNames.Emplace(TEXT("EInventorySlot::EInventorySlot_MAX"), 3);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EInventorySlot");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("Hands.ToolTip"), TEXT("For currently equipped items/weapons"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(ReturnEnum, TEXT("Primary.ToolTip"), TEXT("For primary weapons on spine bone"));
			MetaData->SetValue(ReturnEnum, TEXT("Secondary.ToolTip"), TEXT("Storage for small items like flashlight on pelvis"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_SurvivalGame_EInventorySlot_CRC() { return 139199685U; }
class UScriptStruct* FTakeHitInfo::StaticStruct()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UScriptStruct* Z_Construct_UScriptStruct_FTakeHitInfo();
		extern SURVIVALGAME_API uint32 Get_Z_Construct_UScriptStruct_FTakeHitInfo_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTakeHitInfo, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("TakeHitInfo"), sizeof(FTakeHitInfo), Get_Z_Construct_UScriptStruct_FTakeHitInfo_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTakeHitInfo(FTakeHitInfo::StaticStruct, TEXT("/Script/SurvivalGame"), TEXT("TakeHitInfo"), false, nullptr, nullptr);
static struct FScriptStruct_SurvivalGame_StaticRegisterNativesFTakeHitInfo
{
	FScriptStruct_SurvivalGame_StaticRegisterNativesFTakeHitInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("TakeHitInfo")),new UScriptStruct::TCppStructOps<FTakeHitInfo>);
	}
} ScriptStruct_SurvivalGame_StaticRegisterNativesFTakeHitInfo;
	UScriptStruct* Z_Construct_UScriptStruct_FTakeHitInfo()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UScriptStruct_FTakeHitInfo_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TakeHitInfo"), sizeof(FTakeHitInfo), Get_Z_Construct_UScriptStruct_FTakeHitInfo_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("TakeHitInfo"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FTakeHitInfo>, EStructFlags(0x00000005));
			UProperty* NewProp_RadialDamageEvent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("RadialDamageEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(RadialDamageEvent, FTakeHitInfo), 0x0040008000000000, Z_Construct_UScriptStruct_FRadialDamageEvent());
			UProperty* NewProp_PointDamageEvent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("PointDamageEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(PointDamageEvent, FTakeHitInfo), 0x0040008000000000, Z_Construct_UScriptStruct_FPointDamageEvent());
			UProperty* NewProp_GeneralDamageEvent = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("GeneralDamageEvent"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(GeneralDamageEvent, FTakeHitInfo), 0x0040000000000000, Z_Construct_UScriptStruct_FDamageEvent());
			UProperty* NewProp_EnsureReplicationByte = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EnsureReplicationByte"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(CPP_PROPERTY_BASE(EnsureReplicationByte, FTakeHitInfo), 0x0040000000000000);
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bKilled, FTakeHitInfo, bool);
			UProperty* NewProp_bKilled = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("bKilled"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bKilled, FTakeHitInfo), 0x0010000000000000, CPP_BOOL_PROPERTY_BITMASK(bKilled, FTakeHitInfo), sizeof(bool), true);
			UProperty* NewProp_DamageEventClassID = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("DamageEventClassID"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(CPP_PROPERTY_BASE(DamageEventClassID, FTakeHitInfo), 0x0010000000000000);
			UProperty* NewProp_DamageCauser = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("DamageCauser"), RF_Public|RF_Transient|RF_MarkAsNative) UWeakObjectProperty(CPP_PROPERTY_BASE(DamageCauser, FTakeHitInfo), 0x0014000000000000, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_PawnInstigator = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("PawnInstigator"), RF_Public|RF_Transient|RF_MarkAsNative) UWeakObjectProperty(CPP_PROPERTY_BASE(PawnInstigator, FTakeHitInfo), 0x0014000000000000, Z_Construct_UClass_ASBaseCharacter_NoRegister());
			UProperty* NewProp_DamageTypeClass = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("DamageTypeClass"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(DamageTypeClass, FTakeHitInfo), 0x0010000000000000, Z_Construct_UClass_UObject_NoRegister(), UClass::StaticClass());
			UProperty* NewProp_ActualDamage = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("ActualDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ActualDamage, FTakeHitInfo), 0x0010000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_RadialDamageEvent, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_PointDamageEvent, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_GeneralDamageEvent, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_EnsureReplicationByte, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_bKilled, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_DamageEventClassID, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_DamageCauser, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_PawnInstigator, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_DamageTypeClass, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
			MetaData->SetValue(NewProp_ActualDamage, TEXT("ModuleRelativePath"), TEXT("STypes.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTakeHitInfo_CRC() { return 4009072380U; }
	static FName NAME_ASBaseCharacter_ServerSetSprinting = FName(TEXT("ServerSetSprinting"));
	void ASBaseCharacter::ServerSetSprinting(bool NewSprinting)
	{
		SBaseCharacter_eventServerSetSprinting_Parms Parms;
		Parms.NewSprinting=NewSprinting ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ASBaseCharacter_ServerSetSprinting),&Parms);
	}
	static FName NAME_ASBaseCharacter_ServerSetTargeting = FName(TEXT("ServerSetTargeting"));
	void ASBaseCharacter::ServerSetTargeting(bool NewTargeting)
	{
		SBaseCharacter_eventServerSetTargeting_Parms Parms;
		Parms.NewTargeting=NewTargeting ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ASBaseCharacter_ServerSetTargeting),&Parms);
	}
	void ASBaseCharacter::StaticRegisterNativesASBaseCharacter()
	{
		UClass* Class = ASBaseCharacter::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetAimOffsets", (Native)&ASBaseCharacter::execGetAimOffsets },
			{ "GetHealth", (Native)&ASBaseCharacter::execGetHealth },
			{ "GetMaxHealth", (Native)&ASBaseCharacter::execGetMaxHealth },
			{ "IsAlive", (Native)&ASBaseCharacter::execIsAlive },
			{ "IsSprinting", (Native)&ASBaseCharacter::execIsSprinting },
			{ "IsTargeting", (Native)&ASBaseCharacter::execIsTargeting },
			{ "OnRep_LastTakeHitInfo", (Native)&ASBaseCharacter::execOnRep_LastTakeHitInfo },
			{ "ServerSetSprinting", (Native)&ASBaseCharacter::execServerSetSprinting },
			{ "ServerSetTargeting", (Native)&ASBaseCharacter::execServerSetTargeting },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 9);
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_GetAimOffsets()
	{
		struct SBaseCharacter_eventGetAimOffsets_Parms
		{
			FRotator ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetAimOffsets"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54820401, 65535, sizeof(SBaseCharacter_eventGetAimOffsets_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(ReturnValue, SBaseCharacter_eventGetAimOffsets_Parms), 0x0010000000000580, Z_Construct_UScriptStruct_FRotator());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Targeting"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Retrieve Pitch/Yaw from current camera"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_GetHealth()
	{
		struct SBaseCharacter_eventGetHealth_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetHealth"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SBaseCharacter_eventGetHealth_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SBaseCharacter_eventGetHealth_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_GetMaxHealth()
	{
		struct SBaseCharacter_eventGetMaxHealth_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetMaxHealth"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SBaseCharacter_eventGetMaxHealth_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SBaseCharacter_eventGetMaxHealth_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_IsAlive()
	{
		struct SBaseCharacter_eventIsAlive_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsAlive"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SBaseCharacter_eventIsAlive_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SBaseCharacter_eventIsAlive_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SBaseCharacter_eventIsAlive_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SBaseCharacter_eventIsAlive_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_IsSprinting()
	{
		struct SBaseCharacter_eventIsSprinting_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsSprinting"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020400, 65535, sizeof(SBaseCharacter_eventIsSprinting_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SBaseCharacter_eventIsSprinting_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SBaseCharacter_eventIsSprinting_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SBaseCharacter_eventIsSprinting_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Movement"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_IsTargeting()
	{
		struct SBaseCharacter_eventIsTargeting_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsTargeting"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SBaseCharacter_eventIsTargeting_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SBaseCharacter_eventIsTargeting_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SBaseCharacter_eventIsTargeting_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SBaseCharacter_eventIsTargeting_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Targeting"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Is player aiming down sights"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_OnRep_LastTakeHitInfo()
	{
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_LastTakeHitInfo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00080401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_ServerSetSprinting()
	{
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerSetSprinting"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535, sizeof(SBaseCharacter_eventServerSetSprinting_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(NewSprinting, SBaseCharacter_eventServerSetSprinting_Parms, bool);
			UProperty* NewProp_NewSprinting = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewSprinting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(NewSprinting, SBaseCharacter_eventServerSetSprinting_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(NewSprinting, SBaseCharacter_eventServerSetSprinting_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Server side call to update actual sprint state"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBaseCharacter_ServerSetTargeting()
	{
		UObject* Outer=Z_Construct_UClass_ASBaseCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerSetTargeting"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535, sizeof(SBaseCharacter_eventServerSetTargeting_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(NewTargeting, SBaseCharacter_eventServerSetTargeting_Parms, bool);
			UProperty* NewProp_NewTargeting = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewTargeting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(NewTargeting, SBaseCharacter_eventServerSetTargeting_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(NewTargeting, SBaseCharacter_eventServerSetTargeting_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASBaseCharacter_NoRegister()
	{
		return ASBaseCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_ASBaseCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASBaseCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_GetAimOffsets());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_GetHealth());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_GetMaxHealth());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_IsAlive());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_IsSprinting());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_IsTargeting());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_OnRep_LastTakeHitInfo());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_ServerSetSprinting());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBaseCharacter_ServerSetTargeting());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_LastTakeHitInfo = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("LastTakeHitInfo"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(LastTakeHitInfo, ASBaseCharacter), 0x0020088100002020, Z_Construct_UScriptStruct_FTakeHitInfo());
				NewProp_LastTakeHitInfo->RepNotifyFunc = FName(TEXT("OnRep_LastTakeHitInfo"));
				UProperty* NewProp_Health = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Health"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Health, ASBaseCharacter), 0x0020080000010021);
				UProperty* NewProp_TargetingSpeedModifier = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TargetingSpeedModifier"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(TargetingSpeedModifier, ASBaseCharacter), 0x0020080000010001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsTargeting, ASBaseCharacter, bool);
				UProperty* NewProp_bIsTargeting = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsTargeting"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsTargeting, ASBaseCharacter), 0x0020080000002020, CPP_BOOL_PROPERTY_BITMASK(bIsTargeting, ASBaseCharacter), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bWantsToRun, ASBaseCharacter, bool);
				UProperty* NewProp_bWantsToRun = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bWantsToRun"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bWantsToRun, ASBaseCharacter), 0x0020080000002020, CPP_BOOL_PROPERTY_BITMASK(bWantsToRun, ASBaseCharacter), sizeof(bool), true);
				UProperty* NewProp_SprintingSpeedModifier = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SprintingSpeedModifier"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SprintingSpeedModifier, ASBaseCharacter), 0x0020080000010001);
				UProperty* NewProp_SoundDeath = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundDeath"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundDeath, ASBaseCharacter), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundTakeHit = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundTakeHit"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundTakeHit, ASBaseCharacter), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_GetAimOffsets(), "GetAimOffsets"); // 1136082090
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_GetHealth(), "GetHealth"); // 77280146
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_GetMaxHealth(), "GetMaxHealth"); // 1223735868
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_IsAlive(), "IsAlive"); // 2184997699
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_IsSprinting(), "IsSprinting"); // 757248913
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_IsTargeting(), "IsTargeting"); // 742097116
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_OnRep_LastTakeHitInfo(), "OnRep_LastTakeHitInfo"); // 1718196743
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_ServerSetSprinting(), "ServerSetSprinting"); // 1008770911
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBaseCharacter_ServerSetTargeting(), "ServerSetTargeting"); // 454797050
				static TCppClassTypeInfo<TCppClassTypeTraits<ASBaseCharacter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SBaseCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_LastTakeHitInfo, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_LastTakeHitInfo, TEXT("ToolTip"), TEXT("Holds hit data to replicate hits and death to clients"));
				MetaData->SetValue(NewProp_Health, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_Health, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_TargetingSpeedModifier, TEXT("Category"), TEXT("Targeting"));
				MetaData->SetValue(NewProp_TargetingSpeedModifier, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_bIsTargeting, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_bWantsToRun, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_bWantsToRun, TEXT("ToolTip"), TEXT("Character wants to run, checked during Tick to see if allowed"));
				MetaData->SetValue(NewProp_SprintingSpeedModifier, TEXT("Category"), TEXT("Movement"));
				MetaData->SetValue(NewProp_SprintingSpeedModifier, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_SoundDeath, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundDeath, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
				MetaData->SetValue(NewProp_SoundTakeHit, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundTakeHit, TEXT("ModuleRelativePath"), TEXT("Public/Player/SBaseCharacter.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASBaseCharacter, 1278735130);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASBaseCharacter(Z_Construct_UClass_ASBaseCharacter, &ASBaseCharacter::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASBaseCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASBaseCharacter);
	static FName NAME_ASCharacter_ServerDropWeapon = FName(TEXT("ServerDropWeapon"));
	void ASCharacter::ServerDropWeapon()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASCharacter_ServerDropWeapon),NULL);
	}
	static FName NAME_ASCharacter_ServerEquipWeapon = FName(TEXT("ServerEquipWeapon"));
	void ASCharacter::ServerEquipWeapon(ASWeapon* Weapon)
	{
		SCharacter_eventServerEquipWeapon_Parms Parms;
		Parms.Weapon=Weapon;
		ProcessEvent(FindFunctionChecked(NAME_ASCharacter_ServerEquipWeapon),&Parms);
	}
	static FName NAME_ASCharacter_ServerSetIsJumping = FName(TEXT("ServerSetIsJumping"));
	void ASCharacter::ServerSetIsJumping(bool NewJumping)
	{
		SCharacter_eventServerSetIsJumping_Parms Parms;
		Parms.NewJumping=NewJumping ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ASCharacter_ServerSetIsJumping),&Parms);
	}
	static FName NAME_ASCharacter_ServerUse = FName(TEXT("ServerUse"));
	void ASCharacter::ServerUse()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASCharacter_ServerUse),NULL);
	}
	void ASCharacter::StaticRegisterNativesASCharacter()
	{
		UClass* Class = ASCharacter::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetCurrentWeapon", (Native)&ASCharacter::execGetCurrentWeapon },
			{ "GetHunger", (Native)&ASCharacter::execGetHunger },
			{ "GetLastMakeNoiseTime", (Native)&ASCharacter::execGetLastMakeNoiseTime },
			{ "GetLastNoiseLoudness", (Native)&ASCharacter::execGetLastNoiseLoudness },
			{ "GetMaxHunger", (Native)&ASCharacter::execGetMaxHunger },
			{ "IsFiring", (Native)&ASCharacter::execIsFiring },
			{ "IsInitiatedJump", (Native)&ASCharacter::execIsInitiatedJump },
			{ "MakePawnNoise", (Native)&ASCharacter::execMakePawnNoise },
			{ "OnRep_CurrentWeapon", (Native)&ASCharacter::execOnRep_CurrentWeapon },
			{ "RestoreCondition", (Native)&ASCharacter::execRestoreCondition },
			{ "ServerDropWeapon", (Native)&ASCharacter::execServerDropWeapon },
			{ "ServerEquipWeapon", (Native)&ASCharacter::execServerEquipWeapon },
			{ "ServerSetIsJumping", (Native)&ASCharacter::execServerSetIsJumping },
			{ "ServerUse", (Native)&ASCharacter::execServerUse },
			{ "SwapToNewWeaponMesh", (Native)&ASCharacter::execSwapToNewWeaponMesh },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 15);
	}
	UFunction* Z_Construct_UFunction_ASCharacter_GetCurrentWeapon()
	{
		struct SCharacter_eventGetCurrentWeapon_Parms
		{
			ASWeapon* ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetCurrentWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SCharacter_eventGetCurrentWeapon_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, SCharacter_eventGetCurrentWeapon_Parms), 0x0010000000000580, Z_Construct_UClass_ASWeapon_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_GetHunger()
	{
		struct SCharacter_eventGetHunger_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetHunger"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SCharacter_eventGetHunger_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SCharacter_eventGetHunger_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_GetLastMakeNoiseTime()
	{
		struct SCharacter_eventGetLastMakeNoiseTime_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetLastMakeNoiseTime"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SCharacter_eventGetLastMakeNoiseTime_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SCharacter_eventGetLastMakeNoiseTime_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AI"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_GetLastNoiseLoudness()
	{
		struct SCharacter_eventGetLastNoiseLoudness_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetLastNoiseLoudness"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SCharacter_eventGetLastNoiseLoudness_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SCharacter_eventGetLastNoiseLoudness_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AI"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_GetMaxHunger()
	{
		struct SCharacter_eventGetMaxHunger_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetMaxHunger"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SCharacter_eventGetMaxHunger_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SCharacter_eventGetMaxHunger_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_IsFiring()
	{
		struct SCharacter_eventIsFiring_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsFiring"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SCharacter_eventIsFiring_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SCharacter_eventIsFiring_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SCharacter_eventIsFiring_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SCharacter_eventIsFiring_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_IsInitiatedJump()
	{
		struct SCharacter_eventIsInitiatedJump_Parms
		{
			bool ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("IsInitiatedJump"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SCharacter_eventIsInitiatedJump_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SCharacter_eventIsInitiatedJump_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SCharacter_eventIsInitiatedJump_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SCharacter_eventIsInitiatedJump_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Movement"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_MakePawnNoise()
	{
		struct SCharacter_eventMakePawnNoise_Parms
		{
			float Loudness;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MakePawnNoise"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SCharacter_eventMakePawnNoise_Parms));
			UProperty* NewProp_Loudness = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Loudness"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Loudness, SCharacter_eventMakePawnNoise_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("AI"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("MakeNoise hook to trigger AI noise emitting (Loudness between 0.0-1.0)"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_OnRep_CurrentWeapon()
	{
		struct SCharacter_eventOnRep_CurrentWeapon_Parms
		{
			ASWeapon* LastWeapon;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_CurrentWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00020401, 65535, sizeof(SCharacter_eventOnRep_CurrentWeapon_Parms));
			UProperty* NewProp_LastWeapon = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("LastWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LastWeapon, SCharacter_eventOnRep_CurrentWeapon_Parms), 0x0010000000000080, Z_Construct_UClass_ASWeapon_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("OnRep functions can use a parameter to hold the previous value of the variable. Very useful when you need to handle UnEquip etc."));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_RestoreCondition()
	{
		struct SCharacter_eventRestoreCondition_Parms
		{
			float HealthRestored;
			float HungerRestored;
		};
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RestoreCondition"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SCharacter_eventRestoreCondition_Parms));
			UProperty* NewProp_HungerRestored = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("HungerRestored"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HungerRestored, SCharacter_eventRestoreCondition_Parms), 0x0010000000000080);
			UProperty* NewProp_HealthRestored = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("HealthRestored"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HealthRestored, SCharacter_eventRestoreCondition_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("PlayerCondition"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_ServerDropWeapon()
	{
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerDropWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80240CC1, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_ServerEquipWeapon()
	{
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerEquipWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535, sizeof(SCharacter_eventServerEquipWeapon_Parms));
			UProperty* NewProp_Weapon = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Weapon"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Weapon, SCharacter_eventServerEquipWeapon_Parms), 0x0010000000000080, Z_Construct_UClass_ASWeapon_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_ServerSetIsJumping()
	{
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerSetIsJumping"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535, sizeof(SCharacter_eventServerSetIsJumping_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(NewJumping, SCharacter_eventServerSetIsJumping_Parms, bool);
			UProperty* NewProp_NewJumping = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewJumping"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(NewJumping, SCharacter_eventServerSetIsJumping_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(NewJumping, SCharacter_eventServerSetIsJumping_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_ServerUse()
	{
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerUse"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASCharacter_SwapToNewWeaponMesh()
	{
		UObject* Outer=Z_Construct_UClass_ASCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SwapToNewWeaponMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Animation"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Update the weapon mesh to the newly equipped weapon, this is triggered during an anim montage.\n              NOTE: Requires an AnimNotify created in the Equip animation to tell us when to swap the meshes."));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASCharacter_NoRegister()
	{
		return ASCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_ASCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASBaseCharacter();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_GetCurrentWeapon());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_GetHunger());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_GetLastMakeNoiseTime());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_GetLastNoiseLoudness());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_GetMaxHunger());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_IsFiring());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_IsInitiatedJump());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_MakePawnNoise());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_OnRep_CurrentWeapon());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_RestoreCondition());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_ServerDropWeapon());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_ServerEquipWeapon());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_ServerSetIsJumping());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_ServerUse());
				OuterClass->LinkChild(Z_Construct_UFunction_ASCharacter_SwapToNewWeaponMesh());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_CurrentWeapon = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CurrentWeapon, ASCharacter), 0x0010000100002020, Z_Construct_UClass_ASWeapon_NoRegister());
				NewProp_CurrentWeapon->RepNotifyFunc = FName(TEXT("OnRep_CurrentWeapon"));
				UProperty* NewProp_Inventory = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Inventory"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(Inventory, ASCharacter), 0x0010000000002020);
				UProperty* NewProp_Inventory_Inner = new(EC_InternalUseOnlyConstructor, NewProp_Inventory, TEXT("Inventory"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UClass_ASWeapon_NoRegister());
				UProperty* NewProp_DropWeaponMaxDistance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DropWeaponMaxDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DropWeaponMaxDistance, ASCharacter), 0x0040000000010001);
				UProperty* NewProp_SpineAttachPoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpineAttachPoint"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(SpineAttachPoint, ASCharacter), 0x0040000000010001);
				UProperty* NewProp_PelvisAttachPoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PelvisAttachPoint"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(PelvisAttachPoint, ASCharacter), 0x0040000000010001);
				UProperty* NewProp_WeaponAttachPoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WeaponAttachPoint"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(WeaponAttachPoint, ASCharacter), 0x0040000000010001);
				UProperty* NewProp_HungerDamageType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HungerDamageType"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(HungerDamageType, ASCharacter), 0x0014000000010001, Z_Construct_UClass_UDamageType_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_HungerDamagePerInterval = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HungerDamagePerInterval"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HungerDamagePerInterval, ASCharacter), 0x0010000000010001);
				UProperty* NewProp_MaxHunger = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxHunger"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxHunger, ASCharacter), 0x0010000000010001);
				UProperty* NewProp_Hunger = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Hunger"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Hunger, ASCharacter), 0x0010000000010021);
				UProperty* NewProp_CriticalHungerThreshold = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CriticalHungerThreshold"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(CriticalHungerThreshold, ASCharacter), 0x0010000000000014);
				UProperty* NewProp_IncrementHungerAmount = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IncrementHungerAmount"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(IncrementHungerAmount, ASCharacter), 0x0010000000010001);
				UProperty* NewProp_IncrementHungerInterval = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("IncrementHungerInterval"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(IncrementHungerInterval, ASCharacter), 0x0010000000010001);
				UProperty* NewProp_MaxUseDistance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxUseDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxUseDistance, ASCharacter), 0x0010000000010001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsJumping, ASCharacter, bool);
				UProperty* NewProp_bIsJumping = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsJumping"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsJumping, ASCharacter), 0x0010000000002020, CPP_BOOL_PROPERTY_BITMASK(bIsJumping, ASCharacter), sizeof(bool), true);
				UProperty* NewProp_CarriedObjectComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CarriedObjectComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CarriedObjectComp, ASCharacter), 0x00400000000a0009, Z_Construct_UClass_USCarryObjectComponent_NoRegister());
				UProperty* NewProp_CameraComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CameraComp, ASCharacter), 0x00400000000a0009, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_CameraBoomComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraBoomComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CameraBoomComp, ASCharacter), 0x00400000000a0009, Z_Construct_UClass_USpringArmComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_GetCurrentWeapon(), "GetCurrentWeapon"); // 1063297515
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_GetHunger(), "GetHunger"); // 4190023728
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_GetLastMakeNoiseTime(), "GetLastMakeNoiseTime"); // 238313620
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_GetLastNoiseLoudness(), "GetLastNoiseLoudness"); // 1777108150
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_GetMaxHunger(), "GetMaxHunger"); // 844339871
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_IsFiring(), "IsFiring"); // 4055207681
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_IsInitiatedJump(), "IsInitiatedJump"); // 1405360315
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_MakePawnNoise(), "MakePawnNoise"); // 2074389336
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_OnRep_CurrentWeapon(), "OnRep_CurrentWeapon"); // 2991863514
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_RestoreCondition(), "RestoreCondition"); // 2075970082
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_ServerDropWeapon(), "ServerDropWeapon"); // 1374397317
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_ServerEquipWeapon(), "ServerEquipWeapon"); // 3304631127
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_ServerSetIsJumping(), "ServerSetIsJumping"); // 3548296126
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_ServerUse(), "ServerUse"); // 2414038953
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASCharacter_SwapToNewWeaponMesh(), "SwapToNewWeaponMesh"); // 1596948718
				static TCppClassTypeInfo<TCppClassTypeTraits<ASCharacter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_CurrentWeapon, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_Inventory, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_Inventory, TEXT("ToolTip"), TEXT("All weapons/items the player currently holds"));
				MetaData->SetValue(NewProp_DropWeaponMaxDistance, TEXT("Category"), TEXT("Inventory"));
				MetaData->SetValue(NewProp_DropWeaponMaxDistance, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_DropWeaponMaxDistance, TEXT("ToolTip"), TEXT("Distance away from character when dropping inventory items."));
				MetaData->SetValue(NewProp_SpineAttachPoint, TEXT("Category"), TEXT("Sockets"));
				MetaData->SetValue(NewProp_SpineAttachPoint, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_SpineAttachPoint, TEXT("ToolTip"), TEXT("Attachpoint for primary weapons"));
				MetaData->SetValue(NewProp_PelvisAttachPoint, TEXT("Category"), TEXT("Sockets"));
				MetaData->SetValue(NewProp_PelvisAttachPoint, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_PelvisAttachPoint, TEXT("ToolTip"), TEXT("Attachpoint for items carried on the belt/pelvis."));
				MetaData->SetValue(NewProp_WeaponAttachPoint, TEXT("Category"), TEXT("Sockets"));
				MetaData->SetValue(NewProp_WeaponAttachPoint, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_WeaponAttachPoint, TEXT("ToolTip"), TEXT("Attachpoint for active weapon/item in hands"));
				MetaData->SetValue(NewProp_HungerDamageType, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_HungerDamageType, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_HungerDamageType, TEXT("ToolTip"), TEXT("Damage type applied when player suffers critical hunger"));
				MetaData->SetValue(NewProp_HungerDamagePerInterval, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_HungerDamagePerInterval, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_MaxHunger, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_MaxHunger, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_MaxHunger, TEXT("ToolTip"), TEXT("Documentation Note: MaxHunger does not need to be replicated, only values that change and are displayed or used by clients should ever be replicated."));
				MetaData->SetValue(NewProp_Hunger, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_Hunger, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_CriticalHungerThreshold, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_CriticalHungerThreshold, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_CriticalHungerThreshold, TEXT("ToolTip"), TEXT("Limit when player suffers Hitpoints from extreme hunger"));
				MetaData->SetValue(NewProp_IncrementHungerAmount, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_IncrementHungerAmount, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_IncrementHungerInterval, TEXT("Category"), TEXT("PlayerCondition"));
				MetaData->SetValue(NewProp_IncrementHungerInterval, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_MaxUseDistance, TEXT("Category"), TEXT("ObjectInteraction"));
				MetaData->SetValue(NewProp_MaxUseDistance, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_MaxUseDistance, TEXT("ToolTip"), TEXT("Max distance to use/focus on actors."));
				MetaData->SetValue(NewProp_bIsJumping, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_bIsJumping, TEXT("ToolTip"), TEXT("Is character currently performing a jump action. Resets on landed."));
				MetaData->SetValue(NewProp_CarriedObjectComp, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CarriedObjectComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CarriedObjectComp, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_CameraComp, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CameraComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraComp, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_CameraComp, TEXT("ToolTip"), TEXT("Primary camera of the player"));
				MetaData->SetValue(NewProp_CameraBoomComp, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CameraBoomComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraBoomComp, TEXT("ModuleRelativePath"), TEXT("Public/Player/SCharacter.h"));
				MetaData->SetValue(NewProp_CameraBoomComp, TEXT("ToolTip"), TEXT("Boom to handle distance to player mesh."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASCharacter, 1338447986);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASCharacter(Z_Construct_UClass_ASCharacter, &ASCharacter::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASCharacter);
	static FName NAME_ASZombieCharacter_BroadcastUpdateAudioLoop = FName(TEXT("BroadcastUpdateAudioLoop"));
	void ASZombieCharacter::BroadcastUpdateAudioLoop(bool bNewSensedTarget)
	{
		SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms Parms;
		Parms.bNewSensedTarget=bNewSensedTarget ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_ASZombieCharacter_BroadcastUpdateAudioLoop),&Parms);
	}
	static FName NAME_ASZombieCharacter_SimulateMeleeStrike = FName(TEXT("SimulateMeleeStrike"));
	void ASZombieCharacter::SimulateMeleeStrike()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASZombieCharacter_SimulateMeleeStrike),NULL);
	}
	void ASZombieCharacter::StaticRegisterNativesASZombieCharacter()
	{
		UClass* Class = ASZombieCharacter::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "BroadcastUpdateAudioLoop", (Native)&ASZombieCharacter::execBroadcastUpdateAudioLoop },
			{ "OnHearNoise", (Native)&ASZombieCharacter::execOnHearNoise },
			{ "OnMeleeCompBeginOverlap", (Native)&ASZombieCharacter::execOnMeleeCompBeginOverlap },
			{ "OnSeePlayer", (Native)&ASZombieCharacter::execOnSeePlayer },
			{ "PerformMeleeStrike", (Native)&ASZombieCharacter::execPerformMeleeStrike },
			{ "SimulateMeleeStrike", (Native)&ASZombieCharacter::execSimulateMeleeStrike },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 6);
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_BroadcastUpdateAudioLoop()
	{
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("BroadcastUpdateAudioLoop"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00084CC0, 65535, sizeof(SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bNewSensedTarget, SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms, bool);
			UProperty* NewProp_bNewSensedTarget = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bNewSensedTarget"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bNewSensedTarget, SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(bNewSensedTarget, SZombieCharacter_eventBroadcastUpdateAudioLoop_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Update the vocal loop of the zombie (idle, wandering, hunting)"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_OnHearNoise()
	{
		struct SZombieCharacter_eventOnHearNoise_Parms
		{
			APawn* PawnInstigator;
			FVector Location;
			float Volume;
		};
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnHearNoise"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00C80401, 65535, sizeof(SZombieCharacter_eventOnHearNoise_Parms));
			UProperty* NewProp_Volume = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Volume"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Volume, SZombieCharacter_eventOnHearNoise_Parms), 0x0010000000000080);
			UProperty* NewProp_Location = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Location"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(Location, SZombieCharacter_eventOnHearNoise_Parms), 0x0010000008000182, Z_Construct_UScriptStruct_FVector());
			UProperty* NewProp_PawnInstigator = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("PawnInstigator"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PawnInstigator, SZombieCharacter_eventOnHearNoise_Parms), 0x0010000000000080, Z_Construct_UClass_APawn_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
			MetaData->SetValue(NewProp_Location, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_OnMeleeCompBeginOverlap()
	{
		struct SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms
		{
			UPrimitiveComponent* OverlappedComponent;
			AActor* OtherActor;
			UPrimitiveComponent* OtherComp;
			int32 OtherBodyIndex;
			bool bFromSweep;
			FHitResult SweepResult;
		};
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnMeleeCompBeginOverlap"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00480401, 65535, sizeof(SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms));
			UProperty* NewProp_SweepResult = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SweepResult"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(SweepResult, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010008008000182, Z_Construct_UScriptStruct_FHitResult());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFromSweep, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms, bool);
			UProperty* NewProp_bFromSweep = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bFromSweep"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFromSweep, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010000000000080, CPP_BOOL_PROPERTY_BITMASK(bFromSweep, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), sizeof(bool), true);
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_OverlappedComponent = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OverlappedComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OverlappedComponent, SZombieCharacter_eventOnMeleeCompBeginOverlap_Parms), 0x0010000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("A pawn is in melee range"));
			MetaData->SetValue(NewProp_SweepResult, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
			MetaData->SetValue(NewProp_OverlappedComponent, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_OnSeePlayer()
	{
		struct SZombieCharacter_eventOnSeePlayer_Parms
		{
			APawn* Pawn;
		};
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnSeePlayer"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00080401, 65535, sizeof(SZombieCharacter_eventOnSeePlayer_Parms));
			UProperty* NewProp_Pawn = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Pawn"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Pawn, SZombieCharacter_eventOnSeePlayer_Parms), 0x0010000000000080, Z_Construct_UClass_APawn_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("When using functions as delegates they need to be marked with UFUNCTION(). We assign this function to FSeePawnDelegate"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_PerformMeleeStrike()
	{
		struct SZombieCharacter_eventPerformMeleeStrike_Parms
		{
			AActor* HitActor;
		};
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PerformMeleeStrike"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04080401, 65535, sizeof(SZombieCharacter_eventPerformMeleeStrike_Parms));
			UProperty* NewProp_HitActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("HitActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(HitActor, SZombieCharacter_eventPerformMeleeStrike_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Attacking"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Deal damage to the Actor that was hit by the punch animation"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASZombieCharacter_SimulateMeleeStrike()
	{
		UObject* Outer=Z_Construct_UClass_ASZombieCharacter();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SimulateMeleeStrike"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00084CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASZombieCharacter_NoRegister()
	{
		return ASZombieCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_ASZombieCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASBaseCharacter();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASZombieCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_BroadcastUpdateAudioLoop());
				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_OnHearNoise());
				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_OnMeleeCompBeginOverlap());
				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_OnSeePlayer());
				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_PerformMeleeStrike());
				OuterClass->LinkChild(Z_Construct_UFunction_ASZombieCharacter_SimulateMeleeStrike());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_BehaviorTree = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BehaviorTree"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(BehaviorTree, ASZombieCharacter), 0x0010000000010001, Z_Construct_UClass_UBehaviorTree_NoRegister());
				UProperty* NewProp_BotType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BotType"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(BotType, ASZombieCharacter), 0x0010000000000001, Z_Construct_UEnum_SurvivalGame_EBotBehaviorType());
				UProperty* NewProp_BotType_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_BotType, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsPunching, ASZombieCharacter, bool);
				UProperty* NewProp_bIsPunching = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsPunching"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsPunching, ASZombieCharacter), 0x0010000000000004, CPP_BOOL_PROPERTY_BITMASK(bIsPunching, ASZombieCharacter), sizeof(bool), true);
				UProperty* NewProp_AudioLoopComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AudioLoopComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(AudioLoopComp, ASZombieCharacter), 0x00200800000a0009, Z_Construct_UClass_UAudioComponent_NoRegister());
				UProperty* NewProp_SoundAttackMelee = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundAttackMelee"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundAttackMelee, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundWandering = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundWandering"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundWandering, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundIdle = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundIdle"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundIdle, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundHunting = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundHunting"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundHunting, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundPlayerNoticed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundPlayerNoticed"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundPlayerNoticed, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_MeleeAnimMontage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeleeAnimMontage"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MeleeAnimMontage, ASZombieCharacter), 0x0020080000010001, Z_Construct_UClass_UAnimMontage_NoRegister());
				UProperty* NewProp_MeleeDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeleeDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MeleeDamage, ASZombieCharacter), 0x0020080000010001);
				UProperty* NewProp_PunchDamageType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PunchDamageType"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(PunchDamageType, ASZombieCharacter), 0x0024080000010001, Z_Construct_UClass_UDamageType_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_MeleeCollisionComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeleeCollisionComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MeleeCollisionComp, ASZombieCharacter), 0x00200800000a0009, Z_Construct_UClass_UCapsuleComponent_NoRegister());
				UProperty* NewProp_PawnSensingComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PawnSensingComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PawnSensingComp, ASZombieCharacter), 0x00400000000a0009, Z_Construct_UClass_UPawnSensingComponent_NoRegister());
				UProperty* NewProp_SenseTimeOut = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SenseTimeOut"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(SenseTimeOut, ASZombieCharacter), 0x0040000000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_BroadcastUpdateAudioLoop(), "BroadcastUpdateAudioLoop"); // 2209117185
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_OnHearNoise(), "OnHearNoise"); // 2821408401
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_OnMeleeCompBeginOverlap(), "OnMeleeCompBeginOverlap"); // 1634636652
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_OnSeePlayer(), "OnSeePlayer"); // 2803718236
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_PerformMeleeStrike(), "PerformMeleeStrike"); // 1688829937
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASZombieCharacter_SimulateMeleeStrike(), "SimulateMeleeStrike"); // 1007398842
				static TCppClassTypeInfo<TCppClassTypeTraits<ASZombieCharacter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AI/SZombieCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_BehaviorTree, TEXT("ToolTip"), TEXT("Assigned at the Character level (instead of Controller) so we may use different zombie behaviors while re-using one controller."));
				MetaData->SetValue(NewProp_BotType, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_BotType, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_BotType, TEXT("ToolTip"), TEXT("The bot behavior we want this bot to execute, (passive/patrol) by specifying EditAnywhere we can edit this value per-instance when placed on the map."));
				MetaData->SetValue(NewProp_bIsPunching, TEXT("Category"), TEXT("Attacking"));
				MetaData->SetValue(NewProp_bIsPunching, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_AudioLoopComp, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_AudioLoopComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_AudioLoopComp, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_AudioLoopComp, TEXT("ToolTip"), TEXT("Plays the idle, wandering or hunting sound"));
				MetaData->SetValue(NewProp_SoundAttackMelee, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundAttackMelee, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SoundWandering, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundWandering, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SoundIdle, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundIdle, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SoundHunting, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundHunting, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SoundPlayerNoticed, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundPlayerNoticed, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_MeleeAnimMontage, TEXT("Category"), TEXT("Attacking"));
				MetaData->SetValue(NewProp_MeleeAnimMontage, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_MeleeDamage, TEXT("Category"), TEXT("Attacking"));
				MetaData->SetValue(NewProp_MeleeDamage, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_PunchDamageType, TEXT("Category"), TEXT("Attacking"));
				MetaData->SetValue(NewProp_PunchDamageType, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_MeleeCollisionComp, TEXT("Category"), TEXT("Attacking"));
				MetaData->SetValue(NewProp_MeleeCollisionComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MeleeCollisionComp, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_PawnSensingComp, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_PawnSensingComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_PawnSensingComp, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SenseTimeOut, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_SenseTimeOut, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieCharacter.h"));
				MetaData->SetValue(NewProp_SenseTimeOut, TEXT("ToolTip"), TEXT("Time-out value to clear the sensed position of the player. Should be higher than Sense interval in the PawnSense component not never miss sense ticks."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASZombieCharacter, 473872921);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASZombieCharacter(Z_Construct_UClass_ASZombieCharacter, &ASZombieCharacter::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASZombieCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASZombieCharacter);
	void ASBotWaypoint::StaticRegisterNativesASBotWaypoint()
	{
	}
	UClass* Z_Construct_UClass_ASBotWaypoint_NoRegister()
	{
		return ASBotWaypoint::StaticClass();
	}
	UClass* Z_Construct_UClass_ASBotWaypoint()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ATargetPoint();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASBotWaypoint::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


				static TCppClassTypeInfo<TCppClassTypeTraits<ASBotWaypoint> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AI/SBotWaypoint.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AI/SBotWaypoint.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Waypoint helper for bots to generate waypoints during patrols. Object is placed in level to specify a potential patrol target location."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASBotWaypoint, 3375379634);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASBotWaypoint(Z_Construct_UClass_ASBotWaypoint, &ASBotWaypoint::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASBotWaypoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASBotWaypoint);
	static FName NAME_USCarryObjectComponent_OnDropMulticast = FName(TEXT("OnDropMulticast"));
	void USCarryObjectComponent::OnDropMulticast()
	{
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_OnDropMulticast),NULL);
	}
	static FName NAME_USCarryObjectComponent_OnPickupMulticast = FName(TEXT("OnPickupMulticast"));
	void USCarryObjectComponent::OnPickupMulticast(AActor* FocusActor)
	{
		SCarryObjectComponent_eventOnPickupMulticast_Parms Parms;
		Parms.FocusActor=FocusActor;
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_OnPickupMulticast),&Parms);
	}
	static FName NAME_USCarryObjectComponent_OnRotateMulticast = FName(TEXT("OnRotateMulticast"));
	void USCarryObjectComponent::OnRotateMulticast(float DirectionYaw, float DirectionRoll)
	{
		SCarryObjectComponent_eventOnRotateMulticast_Parms Parms;
		Parms.DirectionYaw=DirectionYaw;
		Parms.DirectionRoll=DirectionRoll;
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_OnRotateMulticast),&Parms);
	}
	static FName NAME_USCarryObjectComponent_ServerDrop = FName(TEXT("ServerDrop"));
	void USCarryObjectComponent::ServerDrop()
	{
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_ServerDrop),NULL);
	}
	static FName NAME_USCarryObjectComponent_ServerPickup = FName(TEXT("ServerPickup"));
	void USCarryObjectComponent::ServerPickup()
	{
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_ServerPickup),NULL);
	}
	static FName NAME_USCarryObjectComponent_ServerRotate = FName(TEXT("ServerRotate"));
	void USCarryObjectComponent::ServerRotate(float DirectionYaw, float DirectionRoll)
	{
		SCarryObjectComponent_eventServerRotate_Parms Parms;
		Parms.DirectionYaw=DirectionYaw;
		Parms.DirectionRoll=DirectionRoll;
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_ServerRotate),&Parms);
	}
	static FName NAME_USCarryObjectComponent_ServerThrow = FName(TEXT("ServerThrow"));
	void USCarryObjectComponent::ServerThrow()
	{
		ProcessEvent(FindFunctionChecked(NAME_USCarryObjectComponent_ServerThrow),NULL);
	}
	void USCarryObjectComponent::StaticRegisterNativesUSCarryObjectComponent()
	{
		UClass* Class = USCarryObjectComponent::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "OnDropMulticast", (Native)&USCarryObjectComponent::execOnDropMulticast },
			{ "OnPickupMulticast", (Native)&USCarryObjectComponent::execOnPickupMulticast },
			{ "OnRotateMulticast", (Native)&USCarryObjectComponent::execOnRotateMulticast },
			{ "ServerDrop", (Native)&USCarryObjectComponent::execServerDrop },
			{ "ServerPickup", (Native)&USCarryObjectComponent::execServerPickup },
			{ "ServerRotate", (Native)&USCarryObjectComponent::execServerRotate },
			{ "ServerThrow", (Native)&USCarryObjectComponent::execServerThrow },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 7);
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnDropMulticast()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnDropMulticast"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00024CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnPickupMulticast()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnPickupMulticast"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00024CC0, 65535, sizeof(SCarryObjectComponent_eventOnPickupMulticast_Parms));
			UProperty* NewProp_FocusActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("FocusActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FocusActor, SCarryObjectComponent_eventOnPickupMulticast_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_OnRotateMulticast()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRotateMulticast"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00024CC0, 65535, sizeof(SCarryObjectComponent_eventOnRotateMulticast_Parms));
			UProperty* NewProp_DirectionRoll = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DirectionRoll"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DirectionRoll, SCarryObjectComponent_eventOnRotateMulticast_Parms), 0x0010000000000080);
			UProperty* NewProp_DirectionYaw = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DirectionYaw"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DirectionYaw, SCarryObjectComponent_eventOnRotateMulticast_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerDrop()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerDrop"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerPickup()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerPickup"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerRotate()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerRotate"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535, sizeof(SCarryObjectComponent_eventServerRotate_Parms));
			UProperty* NewProp_DirectionRoll = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DirectionRoll"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DirectionRoll, SCarryObjectComponent_eventServerRotate_Parms), 0x0010000000000080);
			UProperty* NewProp_DirectionYaw = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("DirectionYaw"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DirectionYaw, SCarryObjectComponent_eventServerRotate_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_USCarryObjectComponent_ServerThrow()
	{
		UObject* Outer=Z_Construct_UClass_USCarryObjectComponent();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerThrow"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80220CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USCarryObjectComponent_NoRegister()
	{
		return USCarryObjectComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_USCarryObjectComponent()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USpringArmComponent();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = USCarryObjectComponent::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B00080;

				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_OnDropMulticast());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_OnPickupMulticast());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_OnRotateMulticast());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_ServerDrop());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_ServerPickup());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_ServerRotate());
				OuterClass->LinkChild(Z_Construct_UFunction_USCarryObjectComponent_ServerThrow());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_MaxPickupDistance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxPickupDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxPickupDistance, USCarryObjectComponent), 0x0010000000010001);
				UProperty* NewProp_RotateSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RotateSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RotateSpeed, USCarryObjectComponent), 0x0020080000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_OnDropMulticast(), "OnDropMulticast"); // 186729732
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_OnPickupMulticast(), "OnPickupMulticast"); // 872614284
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_OnRotateMulticast(), "OnRotateMulticast"); // 1866298567
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_ServerDrop(), "ServerDrop"); // 674967795
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_ServerPickup(), "ServerPickup"); // 182223392
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_ServerRotate(), "ServerRotate"); // 245471701
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_USCarryObjectComponent_ServerThrow(), "ServerThrow"); // 3035701007
				static TCppClassTypeInfo<TCppClassTypeTraits<USCarryObjectComponent> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintSpawnableComponent"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ClassGroupNames"), TEXT("Survival"));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Mobility Trigger PhysicsVolume"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Components/SCarryObjectComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_MaxPickupDistance, TEXT("Category"), TEXT("Pickup"));
				MetaData->SetValue(NewProp_MaxPickupDistance, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
				MetaData->SetValue(NewProp_RotateSpeed, TEXT("Category"), TEXT("Movement"));
				MetaData->SetValue(NewProp_RotateSpeed, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCarryObjectComponent.h"));
				MetaData->SetValue(NewProp_RotateSpeed, TEXT("ToolTip"), TEXT("Rotation speed"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USCarryObjectComponent, 1335743461);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USCarryObjectComponent(Z_Construct_UClass_USCarryObjectComponent, &USCarryObjectComponent::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("USCarryObjectComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USCarryObjectComponent);
	void USCharacterMovementComponent::StaticRegisterNativesUSCharacterMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_USCharacterMovementComponent_NoRegister()
	{
		return USCharacterMovementComponent::StaticClass();
	}
	UClass* Z_Construct_UClass_USCharacterMovementComponent()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UCharacterMovementComponent();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = USCharacterMovementComponent::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20B00084;


				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<USCharacterMovementComponent> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Components/SCharacterMovementComponent.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Components/SCharacterMovementComponent.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USCharacterMovementComponent, 4242674115);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USCharacterMovementComponent(Z_Construct_UClass_USCharacterMovementComponent, &USCharacterMovementComponent::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("USCharacterMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USCharacterMovementComponent);
	void USDamageType::StaticRegisterNativesUSDamageType()
	{
	}
	UClass* Z_Construct_UClass_USDamageType_NoRegister()
	{
		return USDamageType::StaticClass();
	}
	UClass* Z_Construct_UClass_USDamageType()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UDamageType();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = USDamageType::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20110080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_LimbDmgModifier = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("LimbDmgModifier"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(LimbDmgModifier, USDamageType), 0x0040000000010011);
				UProperty* NewProp_HeadDmgModifier = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HeadDmgModifier"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HeadDmgModifier, USDamageType), 0x0040000000010011);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bCanDieFrom, USDamageType, bool);
				UProperty* NewProp_bCanDieFrom = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bCanDieFrom"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bCanDieFrom, USDamageType), 0x0040000000010011, CPP_BOOL_PROPERTY_BITMASK(bCanDieFrom, USDamageType), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<USDamageType> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SDamageType.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SDamageType.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_LimbDmgModifier, TEXT("Category"), TEXT("SDamageType"));
				MetaData->SetValue(NewProp_LimbDmgModifier, TEXT("ModuleRelativePath"), TEXT("Public/Items/SDamageType.h"));
				MetaData->SetValue(NewProp_HeadDmgModifier, TEXT("Category"), TEXT("SDamageType"));
				MetaData->SetValue(NewProp_HeadDmgModifier, TEXT("ModuleRelativePath"), TEXT("Public/Items/SDamageType.h"));
				MetaData->SetValue(NewProp_HeadDmgModifier, TEXT("ToolTip"), TEXT("Damage modifier for headshot damage"));
				MetaData->SetValue(NewProp_bCanDieFrom, TEXT("Category"), TEXT("SDamageType"));
				MetaData->SetValue(NewProp_bCanDieFrom, TEXT("ModuleRelativePath"), TEXT("Public/Items/SDamageType.h"));
				MetaData->SetValue(NewProp_bCanDieFrom, TEXT("ToolTip"), TEXT("Can player die from this damage type (eg. players don't die from hunger)"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USDamageType, 1123894852);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USDamageType(Z_Construct_UClass_USDamageType, &USDamageType::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("USDamageType"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USDamageType);
	static FName NAME_ASMutator_CheckRelevance = FName(TEXT("CheckRelevance"));
	bool ASMutator::CheckRelevance(AActor* Other)
	{
		SMutator_eventCheckRelevance_Parms Parms;
		Parms.Other=Other;
		ProcessEvent(FindFunctionChecked(NAME_ASMutator_CheckRelevance),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_ASMutator_InitGame = FName(TEXT("InitGame"));
	void ASMutator::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
	{
		SMutator_eventInitGame_Parms Parms;
		Parms.MapName=MapName;
		Parms.Options=Options;
		Parms.ErrorMessage=ErrorMessage;
		ProcessEvent(FindFunctionChecked(NAME_ASMutator_InitGame),&Parms);
		ErrorMessage=Parms.ErrorMessage;
	}
	void ASMutator::StaticRegisterNativesASMutator()
	{
		UClass* Class = ASMutator::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "CheckRelevance", (Native)&ASMutator::execCheckRelevance },
			{ "InitGame", (Native)&ASMutator::execInitGame },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_ASMutator_CheckRelevance()
	{
		UObject* Outer=Z_Construct_UClass_ASMutator();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CheckRelevance"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020C04, 65535, sizeof(SMutator_eventCheckRelevance_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SMutator_eventCheckRelevance_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SMutator_eventCheckRelevance_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SMutator_eventCheckRelevance_Parms), sizeof(bool), true);
			UProperty* NewProp_Other = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Other"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Other, SMutator_eventCheckRelevance_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("From UT: entry point for mutators modifying, replacing, or destroying Actors\nreturn false to destroy Other\nnote that certain critical Actors such as PlayerControllers can't be destroyed, but we'll still call this code path to allow mutators\nto change properties on them\nMAKE SURE TO CALL SUPER TO PROCESS ADDITIONAL MUTATORS"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASMutator_InitGame()
	{
		UObject* Outer=Z_Construct_UClass_ASMutator();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("InitGame"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08420C04, 65535, sizeof(SMutator_eventInitGame_Parms));
			UProperty* NewProp_ErrorMessage = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ErrorMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(ErrorMessage, SMutator_eventInitGame_Parms), 0x0010000000000180);
			UProperty* NewProp_Options = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Options"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(Options, SMutator_eventInitGame_Parms), 0x0010000000000080);
			UProperty* NewProp_MapName = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MapName"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(MapName, SMutator_eventInitGame_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator.h"));
			MetaData->SetValue(NewProp_Options, TEXT("NativeConst"), TEXT(""));
			MetaData->SetValue(NewProp_MapName, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASMutator_NoRegister()
	{
		return ASMutator::StaticClass();
	}
	UClass* Z_Construct_UClass_ASMutator()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AInfo();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASMutator::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASMutator_CheckRelevance());
				OuterClass->LinkChild(Z_Construct_UFunction_ASMutator_InitGame());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASMutator_CheckRelevance(), "CheckRelevance"); // 1317975299
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASMutator_InitGame(), "InitGame"); // 1166239994
				static TCppClassTypeInfo<TCppClassTypeTraits<ASMutator> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ChildCanTick"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Mutators/SMutator.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Base Mutator. Can add/modify/remove Actors in a level. Called in a chain by the GameMode class on each Actor during Begin Play of each Actor."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASMutator, 626144195);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASMutator(Z_Construct_UClass_ASMutator, &ASMutator::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASMutator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASMutator);
	static FName NAME_ASGameMode_CheckRelevance = FName(TEXT("CheckRelevance"));
	bool ASGameMode::CheckRelevance(AActor* Other)
	{
		SGameMode_eventCheckRelevance_Parms Parms;
		Parms.Other=Other;
		ProcessEvent(FindFunctionChecked(NAME_ASGameMode_CheckRelevance),&Parms);
		return !!Parms.ReturnValue;
	}
	void ASGameMode::StaticRegisterNativesASGameMode()
	{
		UClass* Class = ASGameMode::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "CheckRelevance", (Native)&ASGameMode::execCheckRelevance },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_ASGameMode_CheckRelevance()
	{
		UObject* Outer=Z_Construct_UClass_ASGameMode();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("CheckRelevance"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08080C04, 65535, sizeof(SGameMode_eventCheckRelevance_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, SGameMode_eventCheckRelevance_Parms, bool);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, SGameMode_eventCheckRelevance_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, SGameMode_eventCheckRelevance_Parms), sizeof(bool), true);
			UProperty* NewProp_Other = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Other"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Other, SGameMode_eventCheckRelevance_Parms), 0x0010000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("From UT Source: Used to modify, remove, and replace Actors. Return false to destroy the passed in Actor. Default implementation queries mutators.\nnote that certain critical Actors such as PlayerControllers can't be destroyed, but we'll still call this code path to allow mutators\nto change properties on them"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASGameMode_NoRegister()
	{
		return ASGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_ASGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameMode();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028C;

				OuterClass->LinkChild(Z_Construct_UFunction_ASGameMode_CheckRelevance());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_MutatorClasses = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MutatorClasses"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(MutatorClasses, ASGameMode), 0x0024080000000001);
				UProperty* NewProp_MutatorClasses_Inner = new(EC_InternalUseOnlyConstructor, NewProp_MutatorClasses, TEXT("MutatorClasses"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0004000000000000, Z_Construct_UClass_ASMutator_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_DefaultInventoryClasses = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DefaultInventoryClasses"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(DefaultInventoryClasses, ASGameMode), 0x0014000000010001);
				UProperty* NewProp_DefaultInventoryClasses_Inner = new(EC_InternalUseOnlyConstructor, NewProp_DefaultInventoryClasses, TEXT("DefaultInventoryClasses"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0004000000000000, Z_Construct_UClass_ASWeapon_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_PrimarySunLight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PrimarySunLight"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PrimarySunLight, ASGameMode), 0x0010000000000004, Z_Construct_UClass_ADirectionalLight_NoRegister());
				UProperty* NewProp_BotPawnClass = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BotPawnClass"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(BotPawnClass, ASGameMode), 0x0024080000010001, Z_Construct_UClass_APawn_NoRegister(), UClass::StaticClass());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bSpawnZombiesAtNight, ASGameMode, bool);
				UProperty* NewProp_bSpawnZombiesAtNight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bSpawnZombiesAtNight"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bSpawnZombiesAtNight, ASGameMode), 0x0020080000010001, CPP_BOOL_PROPERTY_BITMASK(bSpawnZombiesAtNight, ASGameMode), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bAllowFriendlyFireDamage, ASGameMode, bool);
				UProperty* NewProp_bAllowFriendlyFireDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bAllowFriendlyFireDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bAllowFriendlyFireDamage, ASGameMode), 0x0020080000010001, CPP_BOOL_PROPERTY_BITMASK(bAllowFriendlyFireDamage, ASGameMode), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameMode_CheckRelevance(), "CheckRelevance"); // 2774232161
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASGameMode> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/SGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_MutatorClasses, TEXT("Category"), TEXT("Mutators"));
				MetaData->SetValue(NewProp_MutatorClasses, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_MutatorClasses, TEXT("ToolTip"), TEXT("Mutators to create when game starts"));
				MetaData->SetValue(NewProp_DefaultInventoryClasses, TEXT("Category"), TEXT("Player"));
				MetaData->SetValue(NewProp_DefaultInventoryClasses, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_DefaultInventoryClasses, TEXT("ToolTip"), TEXT("The default weapons to spawn with"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("Category"), TEXT("DayNight"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("ToolTip"), TEXT("Primary sun of the level. Assigned in Blueprint during BeginPlay (BlueprintReadWrite is required as tag instead of EditDefaultsOnly)"));
				MetaData->SetValue(NewProp_BotPawnClass, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_BotPawnClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_BotPawnClass, TEXT("ToolTip"), TEXT("The enemy pawn class"));
				MetaData->SetValue(NewProp_bSpawnZombiesAtNight, TEXT("Category"), TEXT("Debug"));
				MetaData->SetValue(NewProp_bSpawnZombiesAtNight, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_bSpawnZombiesAtNight, TEXT("ToolTip"), TEXT("Allow zombie spawns to be disabled (for debugging)"));
				MetaData->SetValue(NewProp_bAllowFriendlyFireDamage, TEXT("Category"), TEXT("Rules"));
				MetaData->SetValue(NewProp_bAllowFriendlyFireDamage, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameMode.h"));
				MetaData->SetValue(NewProp_bAllowFriendlyFireDamage, TEXT("ToolTip"), TEXT("Can we deal damage to players in the same team"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASGameMode, 1137600446);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASGameMode(Z_Construct_UClass_ASGameMode, &ASGameMode::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASGameMode);
	void ASCoopGameMode::StaticRegisterNativesASCoopGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASCoopGameMode_NoRegister()
	{
		return ASCoopGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_ASCoopGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASGameMode();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASCoopGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028D;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_ScoreNightSurvived = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ScoreNightSurvived"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ScoreNightSurvived, ASCoopGameMode), 0x0040000000010001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bSpawnAtTeamPlayer, ASCoopGameMode, bool);
				UProperty* NewProp_bSpawnAtTeamPlayer = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bSpawnAtTeamPlayer"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bSpawnAtTeamPlayer, ASCoopGameMode), 0x0040000000010001, CPP_BOOL_PROPERTY_BITMASK(bSpawnAtTeamPlayer, ASCoopGameMode), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASCoopGameMode> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/SCoopGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SCoopGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_ScoreNightSurvived, TEXT("Category"), TEXT("Scoring"));
				MetaData->SetValue(NewProp_ScoreNightSurvived, TEXT("ModuleRelativePath"), TEXT("Public/World/SCoopGameMode.h"));
				MetaData->SetValue(NewProp_ScoreNightSurvived, TEXT("ToolTip"), TEXT("Points awarded for surviving a night"));
				MetaData->SetValue(NewProp_bSpawnAtTeamPlayer, TEXT("Category"), TEXT("Rules"));
				MetaData->SetValue(NewProp_bSpawnAtTeamPlayer, TEXT("ModuleRelativePath"), TEXT("Public/World/SCoopGameMode.h"));
				MetaData->SetValue(NewProp_bSpawnAtTeamPlayer, TEXT("ToolTip"), TEXT("Spawn at team player if any are alive"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASCoopGameMode, 1557863594);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASCoopGameMode(Z_Construct_UClass_ASCoopGameMode, &ASCoopGameMode::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASCoopGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASCoopGameMode);
	void ASOpenWorldGameMode::StaticRegisterNativesASOpenWorldGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASOpenWorldGameMode_NoRegister()
	{
		return ASOpenWorldGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_ASOpenWorldGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASGameMode();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASOpenWorldGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028D;


				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASOpenWorldGameMode> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/SOpenWorldGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SOpenWorldGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASOpenWorldGameMode, 428493775);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASOpenWorldGameMode(Z_Construct_UClass_ASOpenWorldGameMode, &ASOpenWorldGameMode::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASOpenWorldGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASOpenWorldGameMode);
static UEnum* EHUDState_StaticEnum()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EHUDState();
		Singleton = GetStaticEnum(Z_Construct_UEnum_SurvivalGame_EHUDState, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("EHUDState"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHUDState(EHUDState_StaticEnum, TEXT("/Script/SurvivalGame"), TEXT("EHUDState"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_SurvivalGame_EHUDState()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UEnum_SurvivalGame_EHUDState_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHUDState"), 0, Get_Z_Construct_UEnum_SurvivalGame_EHUDState_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EHUDState"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EHUDState::Playing"), 0);
			EnumNames.Emplace(TEXT("EHUDState::Spectating"), 1);
			EnumNames.Emplace(TEXT("EHUDState::MatchEnd"), 2);
			EnumNames.Emplace(TEXT("EHUDState::EHUDState_MAX"), 3);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EHUDState");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/UI/SHUD.h"));
			MetaData->SetValue(ReturnEnum, TEXT("ToolTip"), TEXT("Expose it to Blueprint using this tag"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_SurvivalGame_EHUDState_CRC() { return 1885110294U; }
	static FName NAME_ASHUD_MessageReceived = FName(TEXT("MessageReceived"));
	void ASHUD::MessageReceived(FText const& TextMessage)
	{
		SHUD_eventMessageReceived_Parms Parms;
		Parms.TextMessage=TextMessage;
		ProcessEvent(FindFunctionChecked(NAME_ASHUD_MessageReceived),&Parms);
	}
	static FName NAME_ASHUD_OnStateChanged = FName(TEXT("OnStateChanged"));
	void ASHUD::OnStateChanged(EHUDState NewState)
	{
		SHUD_eventOnStateChanged_Parms Parms;
		Parms.NewState=NewState;
		ProcessEvent(FindFunctionChecked(NAME_ASHUD_OnStateChanged),&Parms);
	}
	void ASHUD::StaticRegisterNativesASHUD()
	{
		UClass* Class = ASHUD::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetCurrentState", (Native)&ASHUD::execGetCurrentState },
			{ "OnStateChanged", (Native)&ASHUD::execOnStateChanged },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_ASHUD_GetCurrentState()
	{
		struct SHUD_eventGetCurrentState_Parms
		{
			EHUDState ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASHUD();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetCurrentState"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SHUD_eventGetCurrentState_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(ReturnValue, SHUD_eventGetCurrentState_Parms), 0x0010000000000580, Z_Construct_UEnum_SurvivalGame_EHUDState());
			UProperty* NewProp_ReturnValue_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_ReturnValue, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("HUD"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/UI/SHUD.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASHUD_MessageReceived()
	{
		UObject* Outer=Z_Construct_UClass_ASHUD();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MessageReceived"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08420800, 65535, sizeof(SHUD_eventMessageReceived_Parms));
			UProperty* NewProp_TextMessage = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("TextMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UTextProperty(CPP_PROPERTY_BASE(TextMessage, SHUD_eventMessageReceived_Parms), 0x0010000008000182);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("HUDEvents"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/UI/SHUD.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("An event hook to call HUD text events to display in the HUD. Blueprint HUD class must implement how to deal with this event."));
			MetaData->SetValue(NewProp_TextMessage, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASHUD_OnStateChanged()
	{
		UObject* Outer=Z_Construct_UClass_ASHUD();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnStateChanged"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x08020C00, 65535, sizeof(SHUD_eventOnStateChanged_Parms));
			UProperty* NewProp_NewState = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewState"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(NewState, SHUD_eventOnStateChanged_Parms), 0x0010000000000080, Z_Construct_UEnum_SurvivalGame_EHUDState());
			UProperty* NewProp_NewState_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_NewState, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("HUDEvents"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/UI/SHUD.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Event hook to update HUD state (eg. to determine visibility of widgets)"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASHUD_NoRegister()
	{
		return ASHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_ASHUD()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AHUD();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASHUD::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028C;

				OuterClass->LinkChild(Z_Construct_UFunction_ASHUD_GetCurrentState());
				OuterClass->LinkChild(Z_Construct_UFunction_ASHUD_MessageReceived());
				OuterClass->LinkChild(Z_Construct_UFunction_ASHUD_OnStateChanged());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASHUD_GetCurrentState(), "GetCurrentState"); // 3570598623
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASHUD_MessageReceived(), "MessageReceived"); // 511316913
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASHUD_OnStateChanged(), "OnStateChanged"); // 3479096088
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASHUD> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Rendering Actor Input Replication"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("UI/SHUD.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/UI/SHUD.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASHUD, 3120705482);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASHUD(Z_Construct_UClass_ASHUD, &ASHUD::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASHUD);
static UEnum* EHUDMessage_StaticEnum()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EHUDMessage();
		Singleton = GetStaticEnum(Z_Construct_UEnum_SurvivalGame_EHUDMessage, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("EHUDMessage"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EHUDMessage(EHUDMessage_StaticEnum, TEXT("/Script/SurvivalGame"), TEXT("EHUDMessage"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_SurvivalGame_EHUDMessage()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UEnum_SurvivalGame_EHUDMessage_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EHUDMessage"), 0, Get_Z_Construct_UEnum_SurvivalGame_EHUDMessage_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EHUDMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EHUDMessage::Weapon_SlotTaken"), 0);
			EnumNames.Emplace(TEXT("EHUDMessage::Character_EnergyRestored"), 1);
			EnumNames.Emplace(TEXT("EHUDMessage::Game_SurviveStart"), 2);
			EnumNames.Emplace(TEXT("EHUDMessage::Game_SurviveEnded"), 3);
			EnumNames.Emplace(TEXT("EHUDMessage::None"), 4);
			EnumNames.Emplace(TEXT("EHUDMessage::EHUDMessage_MAX"), 5);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EHUDMessage");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("Character_EnergyRestored.ToolTip"), TEXT("Character"));
			MetaData->SetValue(ReturnEnum, TEXT("Game_SurviveStart.ToolTip"), TEXT("Gamemode"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
			MetaData->SetValue(ReturnEnum, TEXT("None.ToolTip"), TEXT("No category specified"));
			MetaData->SetValue(ReturnEnum, TEXT("Weapon_SlotTaken.ToolTip"), TEXT("Weapons"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_SurvivalGame_EHUDMessage_CRC() { return 3782995286U; }
	static FName NAME_ASPlayerController_ClientHUDMessage = FName(TEXT("ClientHUDMessage"));
	void ASPlayerController::ClientHUDMessage(EHUDMessage MessageID)
	{
		SPlayerController_eventClientHUDMessage_Parms Parms;
		Parms.MessageID=MessageID;
		ProcessEvent(FindFunctionChecked(NAME_ASPlayerController_ClientHUDMessage),&Parms);
	}
	static FName NAME_ASPlayerController_ClientHUDStateChanged = FName(TEXT("ClientHUDStateChanged"));
	void ASPlayerController::ClientHUDStateChanged(EHUDState NewState)
	{
		SPlayerController_eventClientHUDStateChanged_Parms Parms;
		Parms.NewState=NewState;
		ProcessEvent(FindFunctionChecked(NAME_ASPlayerController_ClientHUDStateChanged),&Parms);
	}
	static FName NAME_ASPlayerController_ServerSuicide = FName(TEXT("ServerSuicide"));
	void ASPlayerController::ServerSuicide()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASPlayerController_ServerSuicide),NULL);
	}
	void ASPlayerController::StaticRegisterNativesASPlayerController()
	{
		UClass* Class = ASPlayerController::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ClientHUDMessage", (Native)&ASPlayerController::execClientHUDMessage },
			{ "ClientHUDStateChanged", (Native)&ASPlayerController::execClientHUDStateChanged },
			{ "ServerSuicide", (Native)&ASPlayerController::execServerSuicide },
			{ "Suicide", (Native)&ASPlayerController::execSuicide },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 4);
	}
	UFunction* Z_Construct_UFunction_ASPlayerController_ClientHUDMessage()
	{
		UObject* Outer=Z_Construct_UClass_ASPlayerController();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ClientHUDMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x01020CC0, 65535, sizeof(SPlayerController_eventClientHUDMessage_Parms));
			UProperty* NewProp_MessageID = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("MessageID"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(MessageID, SPlayerController_eventClientHUDMessage_Parms), 0x0010000000000080, Z_Construct_UEnum_SurvivalGame_EHUDMessage());
			UProperty* NewProp_MessageID_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_MessageID, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Enum is remapped to localized text before sending it to the HUD"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerController_ClientHUDStateChanged()
	{
		UObject* Outer=Z_Construct_UClass_ASPlayerController();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ClientHUDStateChanged"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x01020CC0, 65535, sizeof(SPlayerController_eventClientHUDStateChanged_Parms));
			UProperty* NewProp_NewState = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewState"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(NewState, SPlayerController_eventClientHUDStateChanged_Parms), 0x0010000000000080, Z_Construct_UEnum_SurvivalGame_EHUDState());
			UProperty* NewProp_NewState_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_NewState, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerController_ServerSuicide()
	{
		UObject* Outer=Z_Construct_UClass_ASPlayerController();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerSuicide"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80240CC1, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerController_Suicide()
	{
		UObject* Outer=Z_Construct_UClass_ASPlayerController();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Suicide"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00020600, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Kill the current pawn"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASPlayerController_NoRegister()
	{
		return ASPlayerController::StaticClass();
	}
	UClass* Z_Construct_UClass_ASPlayerController()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APlayerController();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASPlayerController::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900284;

				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerController_ClientHUDMessage());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerController_ClientHUDStateChanged());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerController_ServerSuicide());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerController_Suicide());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bRespawnImmediately, ASPlayerController, bool);
				UProperty* NewProp_bRespawnImmediately = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bRespawnImmediately"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bRespawnImmediately, ASPlayerController), 0x0040000000010001, CPP_BOOL_PROPERTY_BITMASK(bRespawnImmediately, ASPlayerController), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerController_ClientHUDMessage(), "ClientHUDMessage"); // 3963218055
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerController_ClientHUDStateChanged(), "ClientHUDStateChanged"); // 1460054558
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerController_ServerSuicide(), "ServerSuicide"); // 2439032928
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerController_Suicide(), "Suicide"); // 4148076390
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				static TCppClassTypeInfo<TCppClassTypeTraits<ASPlayerController> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SPlayerController.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_bRespawnImmediately, TEXT("Category"), TEXT("Spawning"));
				MetaData->SetValue(NewProp_bRespawnImmediately, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerController.h"));
				MetaData->SetValue(NewProp_bRespawnImmediately, TEXT("ToolTip"), TEXT("Flag to respawn or start spectating upon death"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASPlayerController, 4130462687);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASPlayerController(Z_Construct_UClass_ASPlayerController, &ASPlayerController::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASPlayerController);
	static FName NAME_ASGameState_BroadcastGameMessage = FName(TEXT("BroadcastGameMessage"));
	void ASGameState::BroadcastGameMessage(EHUDMessage NewMessage)
	{
		SGameState_eventBroadcastGameMessage_Parms Parms;
		Parms.NewMessage=NewMessage;
		ProcessEvent(FindFunctionChecked(NAME_ASGameState_BroadcastGameMessage),&Parms);
	}
	void ASGameState::StaticRegisterNativesASGameState()
	{
		UClass* Class = ASGameState::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "BroadcastGameMessage", (Native)&ASGameState::execBroadcastGameMessage },
			{ "GetElapsedDays", (Native)&ASGameState::execGetElapsedDays },
			{ "GetElapsedFullDaysInMinutes", (Native)&ASGameState::execGetElapsedFullDaysInMinutes },
			{ "GetRealSecondsTillSunrise", (Native)&ASGameState::execGetRealSecondsTillSunrise },
			{ "GetTotalScore", (Native)&ASGameState::execGetTotalScore },
			{ "SetTimeOfDay", (Native)&ASGameState::execSetTimeOfDay },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 6);
	}
	UFunction* Z_Construct_UFunction_ASGameState_BroadcastGameMessage()
	{
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("BroadcastGameMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00024CC0, 65535, sizeof(SGameState_eventBroadcastGameMessage_Parms));
			UProperty* NewProp_NewMessage = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewMessage"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(NewMessage, SGameState_eventBroadcastGameMessage_Parms), 0x0010000000000080, Z_Construct_UEnum_SurvivalGame_EHUDMessage());
			UProperty* NewProp_NewMessage_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_NewMessage, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("NetMulticast will send this event to all clients that know about this object, in the case of GameState that means every client."));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASGameState_GetElapsedDays()
	{
		struct SGameState_eventGetElapsedDays_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetElapsedDays"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SGameState_eventGetElapsedDays_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SGameState_eventGetElapsedDays_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("TimeOfDay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASGameState_GetElapsedFullDaysInMinutes()
	{
		struct SGameState_eventGetElapsedFullDaysInMinutes_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetElapsedFullDaysInMinutes"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SGameState_eventGetElapsedFullDaysInMinutes_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SGameState_eventGetElapsedFullDaysInMinutes_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("TimeOfDay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Returns whole days elapsed, represented in minutes"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASGameState_GetRealSecondsTillSunrise()
	{
		struct SGameState_eventGetRealSecondsTillSunrise_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetRealSecondsTillSunrise"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SGameState_eventGetRealSecondsTillSunrise_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SGameState_eventGetRealSecondsTillSunrise_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("TimeOfDay"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Return the time in real seconds till next sunrise"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASGameState_GetTotalScore()
	{
		struct SGameState_eventGetTotalScore_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetTotalScore"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x04020401, 65535, sizeof(SGameState_eventGetTotalScore_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SGameState_eventGetTotalScore_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Score"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASGameState_SetTimeOfDay()
	{
		struct SGameState_eventSetTimeOfDay_Parms
		{
			float NewTimeOfDay;
		};
		UObject* Outer=Z_Construct_UClass_ASGameState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SetTimeOfDay"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00020601, 65535, sizeof(SGameState_eventSetTimeOfDay_Parms));
			UProperty* NewProp_NewTimeOfDay = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("NewTimeOfDay"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(NewTimeOfDay, SGameState_eventSetTimeOfDay_Parms), 0x0010000000000080);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("By passing in \"exec\" we expose it as a command line (press ~ to open)"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASGameState_NoRegister()
	{
		return ASGameState::StaticClass();
	}
	UClass* Z_Construct_UClass_ASGameState()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameState();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASGameState::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900280;

				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_BroadcastGameMessage());
				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_GetElapsedDays());
				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_GetElapsedFullDaysInMinutes());
				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_GetRealSecondsTillSunrise());
				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_GetTotalScore());
				OuterClass->LinkChild(Z_Construct_UFunction_ASGameState_SetTimeOfDay());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_TimeScale = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TimeScale"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(TimeScale, ASGameState), 0x0010000000010001);
				UProperty* NewProp_ElapsedGameMinutes = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ElapsedGameMinutes"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ElapsedGameMinutes, ASGameState), 0x0010000000000034);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsNight, ASGameState, bool);
				UProperty* NewProp_bIsNight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsNight"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsNight, ASGameState), 0x0010000000000020, CPP_BOOL_PROPERTY_BITMASK(bIsNight, ASGameState), sizeof(bool), true);
				UProperty* NewProp_TotalScore = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TotalScore"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(TotalScore, ASGameState), 0x0040000000000020);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_BroadcastGameMessage(), "BroadcastGameMessage"); // 1452706612
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_GetElapsedDays(), "GetElapsedDays"); // 1380075742
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_GetElapsedFullDaysInMinutes(), "GetElapsedFullDaysInMinutes"); // 4164347845
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_GetRealSecondsTillSunrise(), "GetRealSecondsTillSunrise"); // 4226506573
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_GetTotalScore(), "GetTotalScore"); // 118249816
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASGameState_SetTimeOfDay(), "SetTimeOfDay"); // 3309842738
				static TCppClassTypeInfo<TCppClassTypeTraits<ASGameState> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/SGameState.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_TimeScale, TEXT("Category"), TEXT("TimeOfDay"));
				MetaData->SetValue(NewProp_TimeScale, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
				MetaData->SetValue(NewProp_TimeScale, TEXT("ToolTip"), TEXT("Conversion of 1 second real time to X seconds gametime of the day/night cycle"));
				MetaData->SetValue(NewProp_ElapsedGameMinutes, TEXT("Category"), TEXT("TimeOfDay"));
				MetaData->SetValue(NewProp_ElapsedGameMinutes, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
				MetaData->SetValue(NewProp_ElapsedGameMinutes, TEXT("ToolTip"), TEXT("Current time of day in the gamemode represented in full minutes"));
				MetaData->SetValue(NewProp_bIsNight, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
				MetaData->SetValue(NewProp_TotalScore, TEXT("ModuleRelativePath"), TEXT("Public/World/SGameState.h"));
				MetaData->SetValue(NewProp_TotalScore, TEXT("ToolTip"), TEXT("Total accumulated score from all players"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASGameState, 3940159287);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASGameState(Z_Construct_UClass_ASGameState, &ASGameState::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASGameState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASGameState);
	void ASImpactEffect::StaticRegisterNativesASImpactEffect()
	{
	}
	UClass* Z_Construct_UClass_ASImpactEffect_NoRegister()
	{
		return ASImpactEffect::StaticClass();
	}
	UClass* Z_Construct_UClass_ASImpactEffect()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASImpactEffect::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_DecalLifeSpan = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DecalLifeSpan"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DecalLifeSpan, ASImpactEffect), 0x0010000000010001);
				UProperty* NewProp_DecalSize = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DecalSize"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(DecalSize, ASImpactEffect), 0x0010000000010001);
				UProperty* NewProp_DecalMaterial = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DecalMaterial"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(DecalMaterial, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_UMaterial_NoRegister());
				UProperty* NewProp_ZombieFleshSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ZombieFleshSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ZombieFleshSound, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_PlayerFleshSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PlayerFleshSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PlayerFleshSound, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_DefaultSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DefaultSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(DefaultSound, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_ZombieFleshFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ZombieFleshFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ZombieFleshFX, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_PlayerFleshFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PlayerFleshFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PlayerFleshFX, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_DefaultFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DefaultFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(DefaultFX, ASImpactEffect), 0x0010000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASImpactEffect> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SImpactEffect.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DecalLifeSpan, TEXT("Category"), TEXT("Decal"));
				MetaData->SetValue(NewProp_DecalLifeSpan, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DecalSize, TEXT("Category"), TEXT("Decal"));
				MetaData->SetValue(NewProp_DecalSize, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DecalMaterial, TEXT("Category"), TEXT("Decal"));
				MetaData->SetValue(NewProp_DecalMaterial, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_ZombieFleshSound, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_ZombieFleshSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_PlayerFleshSound, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_PlayerFleshSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DefaultSound, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_DefaultSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_ZombieFleshFX, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_ZombieFleshFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_PlayerFleshFX, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_PlayerFleshFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DefaultFX, TEXT("Category"), TEXT("SImpactEffect"));
				MetaData->SetValue(NewProp_DefaultFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SImpactEffect.h"));
				MetaData->SetValue(NewProp_DefaultFX, TEXT("ToolTip"), TEXT("FX spawned on standard materials"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASImpactEffect, 3393089272);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASImpactEffect(Z_Construct_UClass_ASImpactEffect, &ASImpactEffect::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASImpactEffect"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASImpactEffect);
	void USLocalPlayer::StaticRegisterNativesUSLocalPlayer()
	{
	}
	UClass* Z_Construct_UClass_USLocalPlayer_NoRegister()
	{
		return USLocalPlayer::StaticClass();
	}
	UClass* Z_Construct_UClass_USLocalPlayer()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ULocalPlayer();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = USLocalPlayer::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2010008C;


				OuterClass->ClassConfigName = FName(TEXT("Engine"));
				static TCppClassTypeInfo<TCppClassTypeTraits<USLocalPlayer> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SLocalPlayer.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SLocalPlayer.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("LocalPlayer defines a specific user when dealing with multiple players on a single machine (eg. local split-screen)"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USLocalPlayer, 2672116648);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USLocalPlayer(Z_Construct_UClass_USLocalPlayer, &USLocalPlayer::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("USLocalPlayer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USLocalPlayer);
class UScriptStruct* FReplacementInfo::StaticStruct()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UScriptStruct* Z_Construct_UScriptStruct_FReplacementInfo();
		extern SURVIVALGAME_API uint32 Get_Z_Construct_UScriptStruct_FReplacementInfo_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FReplacementInfo, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("ReplacementInfo"), sizeof(FReplacementInfo), Get_Z_Construct_UScriptStruct_FReplacementInfo_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FReplacementInfo(FReplacementInfo::StaticStruct, TEXT("/Script/SurvivalGame"), TEXT("ReplacementInfo"), false, nullptr, nullptr);
static struct FScriptStruct_SurvivalGame_StaticRegisterNativesFReplacementInfo
{
	FScriptStruct_SurvivalGame_StaticRegisterNativesFReplacementInfo()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("ReplacementInfo")),new UScriptStruct::TCppStructOps<FReplacementInfo>);
	}
} ScriptStruct_SurvivalGame_StaticRegisterNativesFReplacementInfo;
	UScriptStruct* Z_Construct_UScriptStruct_FReplacementInfo()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UScriptStruct_FReplacementInfo_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ReplacementInfo"), sizeof(FReplacementInfo), Get_Z_Construct_UScriptStruct_FReplacementInfo_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ReplacementInfo"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FReplacementInfo>, EStructFlags(0x00000001));
			UProperty* NewProp_ToWeapon = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("ToWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(ToWeapon, FReplacementInfo), 0x0014000000000005, Z_Construct_UClass_ASWeapon_NoRegister(), UClass::StaticClass());
			UProperty* NewProp_FromWeapon = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("FromWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(FromWeapon, FReplacementInfo), 0x0014000000000005, Z_Construct_UClass_ASWeapon_NoRegister(), UClass::StaticClass());
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator_WeaponReplacement.h"));
			MetaData->SetValue(NewProp_ToWeapon, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(NewProp_ToWeapon, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator_WeaponReplacement.h"));
			MetaData->SetValue(NewProp_ToWeapon, TEXT("ToolTip"), TEXT("fully qualified path of the class to replace it with"));
			MetaData->SetValue(NewProp_FromWeapon, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(NewProp_FromWeapon, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator_WeaponReplacement.h"));
			MetaData->SetValue(NewProp_FromWeapon, TEXT("ToolTip"), TEXT("class name of the weapon we want to get rid of"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FReplacementInfo_CRC() { return 469997998U; }
	void ASMutator_WeaponReplacement::StaticRegisterNativesASMutator_WeaponReplacement()
	{
	}
	UClass* Z_Construct_UClass_ASMutator_WeaponReplacement_NoRegister()
	{
		return ASMutator_WeaponReplacement::StaticClass();
	}
	UClass* Z_Construct_UClass_ASMutator_WeaponReplacement()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASMutator();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASMutator_WeaponReplacement::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_WeaponsToReplace = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WeaponsToReplace"), RF_Public|RF_Transient|RF_MarkAsNative) UArrayProperty(CPP_PROPERTY_BASE(WeaponsToReplace, ASMutator_WeaponReplacement), 0x0010000000010001);
				UProperty* NewProp_WeaponsToReplace_Inner = new(EC_InternalUseOnlyConstructor, NewProp_WeaponsToReplace, TEXT("WeaponsToReplace"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000, Z_Construct_UScriptStruct_FReplacementInfo());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASMutator_WeaponReplacement> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Mutators/SMutator_WeaponReplacement.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator_WeaponReplacement.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Allows mutators to replace weapon pickups in the active level"));
				MetaData->SetValue(NewProp_WeaponsToReplace, TEXT("Category"), TEXT("WeaponReplacement"));
				MetaData->SetValue(NewProp_WeaponsToReplace, TEXT("ModuleRelativePath"), TEXT("Public/Mutators/SMutator_WeaponReplacement.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASMutator_WeaponReplacement, 2478392887);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASMutator_WeaponReplacement(Z_Construct_UClass_ASMutator_WeaponReplacement, &ASMutator_WeaponReplacement::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASMutator_WeaponReplacement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASMutator_WeaponReplacement);
	void USoundNodeLocalPlayer::StaticRegisterNativesUSoundNodeLocalPlayer()
	{
	}
	UClass* Z_Construct_UClass_USoundNodeLocalPlayer_NoRegister()
	{
		return USoundNodeLocalPlayer::StaticClass();
	}
	UClass* Z_Construct_UClass_USoundNodeLocalPlayer()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_USoundNode();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = USoundNodeLocalPlayer::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20101080;


				static TCppClassTypeInfo<TCppClassTypeTraits<USoundNodeLocalPlayer> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Object Object"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Editor/SoundNodeLocalPlayer.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Editor/SoundNodeLocalPlayer.h"));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Choose different branch for sounds attached to locally controlled player\n\nOriginates from ShooterGame project by Epic Games."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundNodeLocalPlayer, 2448483909);
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundNodeLocalPlayer(Z_Construct_UClass_USoundNodeLocalPlayer, &USoundNodeLocalPlayer::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("USoundNodeLocalPlayer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundNodeLocalPlayer);
	void ASPlayerCameraManager::StaticRegisterNativesASPlayerCameraManager()
	{
	}
	UClass* Z_Construct_UClass_ASPlayerCameraManager_NoRegister()
	{
		return ASPlayerCameraManager::StaticClass();
	}
	UClass* Z_Construct_UClass_ASPlayerCameraManager()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APlayerCameraManager();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASPlayerCameraManager::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900288;


				static TCppClassTypeInfo<TCppClassTypeTraits<ASPlayerCameraManager> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SPlayerCameraManager.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerCameraManager.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASPlayerCameraManager, 3819858616);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASPlayerCameraManager(Z_Construct_UClass_ASPlayerCameraManager, &ASPlayerCameraManager::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASPlayerCameraManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASPlayerCameraManager);
	void ASPlayerStart::StaticRegisterNativesASPlayerStart()
	{
	}
	UClass* Z_Construct_UClass_ASPlayerStart_NoRegister()
	{
		return ASPlayerStart::StaticClass();
	}
	UClass* Z_Construct_UClass_ASPlayerStart()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APlayerStart();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASPlayerStart::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bPlayerOnly, ASPlayerStart, bool);
				UProperty* NewProp_bPlayerOnly = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bPlayerOnly"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bPlayerOnly, ASPlayerStart), 0x0040000000000001, CPP_BOOL_PROPERTY_BITMASK(bPlayerOnly, ASPlayerStart), sizeof(bool), true);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASPlayerStart> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Collision Lighting LightColor Force"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/SPlayerStart.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/SPlayerStart.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_bPlayerOnly, TEXT("Category"), TEXT("PlayerStart"));
				MetaData->SetValue(NewProp_bPlayerOnly, TEXT("ModuleRelativePath"), TEXT("Public/World/SPlayerStart.h"));
				MetaData->SetValue(NewProp_bPlayerOnly, TEXT("ToolTip"), TEXT("Is only useable by players - automatically a preferred spawn for players"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASPlayerStart, 206097549);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASPlayerStart(Z_Construct_UClass_ASPlayerStart, &ASPlayerStart::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASPlayerStart"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASPlayerStart);
	void ASPlayerState::StaticRegisterNativesASPlayerState()
	{
		UClass* Class = ASPlayerState::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "GetDeaths", (Native)&ASPlayerState::execGetDeaths },
			{ "GetKills", (Native)&ASPlayerState::execGetKills },
			{ "GetScore", (Native)&ASPlayerState::execGetScore },
			{ "GetTeamNumber", (Native)&ASPlayerState::execGetTeamNumber },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 4);
	}
	UFunction* Z_Construct_UFunction_ASPlayerState_GetDeaths()
	{
		struct SPlayerState_eventGetDeaths_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASPlayerState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetDeaths"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SPlayerState_eventGetDeaths_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SPlayerState_eventGetDeaths_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Score"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerState_GetKills()
	{
		struct SPlayerState_eventGetKills_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASPlayerState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetKills"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SPlayerState_eventGetKills_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SPlayerState_eventGetKills_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Score"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerState_GetScore()
	{
		struct SPlayerState_eventGetScore_Parms
		{
			float ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASPlayerState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetScore"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SPlayerState_eventGetScore_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ReturnValue, SPlayerState_eventGetScore_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Score"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASPlayerState_GetTeamNumber()
	{
		struct SPlayerState_eventGetTeamNumber_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASPlayerState();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetTeamNumber"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SPlayerState_eventGetTeamNumber_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SPlayerState_eventGetTeamNumber_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Teams"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASPlayerState_NoRegister()
	{
		return ASPlayerState::StaticClass();
	}
	UClass* Z_Construct_UClass_ASPlayerState()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_APlayerState();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASPlayerState::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900280;

				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerState_GetDeaths());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerState_GetKills());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerState_GetScore());
				OuterClass->LinkChild(Z_Construct_UFunction_ASPlayerState_GetTeamNumber());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_TeamNumber = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TeamNumber"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(TeamNumber, ASPlayerState), 0x0040000000002020);
				UProperty* NewProp_NumDeaths = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NumDeaths"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(NumDeaths, ASPlayerState), 0x0040000000002020);
				UProperty* NewProp_NumKills = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NumKills"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(NumKills, ASPlayerState), 0x0040000000002020);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerState_GetDeaths(), "GetDeaths"); // 1983761084
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerState_GetKills(), "GetKills"); // 1827473814
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerState_GetScore(), "GetScore"); // 2629494951
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPlayerState_GetTeamNumber(), "GetTeamNumber"); // 3537389515
				static TCppClassTypeInfo<TCppClassTypeTraits<ASPlayerState> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SPlayerState.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
				MetaData->SetValue(NewProp_TeamNumber, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
				MetaData->SetValue(NewProp_TeamNumber, TEXT("ToolTip"), TEXT("Team number assigned to player"));
				MetaData->SetValue(NewProp_NumDeaths, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
				MetaData->SetValue(NewProp_NumKills, TEXT("ModuleRelativePath"), TEXT("Public/Player/SPlayerState.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASPlayerState, 240646917);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASPlayerState(Z_Construct_UClass_ASPlayerState, &ASPlayerState::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASPlayerState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASPlayerState);
	void ASSpectatorPawn::StaticRegisterNativesASSpectatorPawn()
	{
	}
	UClass* Z_Construct_UClass_ASSpectatorPawn_NoRegister()
	{
		return ASSpectatorPawn::StaticClass();
	}
	UClass* Z_Construct_UClass_ASSpectatorPawn()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASpectatorPawn();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASSpectatorPawn::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900280;


				static TCppClassTypeInfo<TCppClassTypeTraits<ASSpectatorPawn> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Player/SSpectatorPawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Player/SSpectatorPawn.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASSpectatorPawn, 2268987281);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASSpectatorPawn(Z_Construct_UClass_ASSpectatorPawn, &ASSpectatorPawn::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASSpectatorPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASSpectatorPawn);
	void ASTimeOfDayManager::StaticRegisterNativesASTimeOfDayManager()
	{
	}
	UClass* Z_Construct_UClass_ASTimeOfDayManager_NoRegister()
	{
		return ASTimeOfDayManager::StaticClass();
	}
	UClass* Z_Construct_UClass_ASTimeOfDayManager()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASTimeOfDayManager::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_AmbientNight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AmbientNight"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(AmbientNight, ASTimeOfDayManager), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_AmbientDaytime = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AmbientDaytime"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(AmbientDaytime, ASTimeOfDayManager), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundNightEnded = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundNightEnded"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundNightEnded, ASTimeOfDayManager), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SoundNightStarted = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SoundNightStarted"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SoundNightStarted, ASTimeOfDayManager), 0x0010000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_SkyLightActor = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SkyLightActor"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SkyLightActor, ASTimeOfDayManager), 0x0010000000000004, Z_Construct_UClass_ASkyLight_NoRegister());
				UProperty* NewProp_PrimarySunLight = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PrimarySunLight"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PrimarySunLight, ASTimeOfDayManager), 0x0010000000000004, Z_Construct_UClass_ADirectionalLight_NoRegister());
				UProperty* NewProp_AmbientAudioComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AmbientAudioComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(AmbientAudioComp, ASTimeOfDayManager), 0x0010000000090009, Z_Construct_UClass_UAudioComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASTimeOfDayManager> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("World/STimeOfDayManager.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_AmbientNight, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_AmbientNight, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_AmbientDaytime, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_AmbientDaytime, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_SoundNightEnded, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundNightEnded, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_SoundNightStarted, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_SoundNightStarted, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_SkyLightActor, TEXT("Category"), TEXT("DayNight"));
				MetaData->SetValue(NewProp_SkyLightActor, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("Category"), TEXT("DayNight"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
				MetaData->SetValue(NewProp_PrimarySunLight, TEXT("ToolTip"), TEXT("Primary sun of the level. Assigned in Blueprint during BeginPlay (BlueprintReadWrite is required as tag instead of EditDefaultsOnly)"));
				MetaData->SetValue(NewProp_AmbientAudioComp, TEXT("Category"), TEXT("Components"));
				MetaData->SetValue(NewProp_AmbientAudioComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_AmbientAudioComp, TEXT("ModuleRelativePath"), TEXT("Public/World/STimeOfDayManager.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASTimeOfDayManager, 1446233476);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASTimeOfDayManager(Z_Construct_UClass_ASTimeOfDayManager, &ASTimeOfDayManager::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASTimeOfDayManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASTimeOfDayManager);
	void ASUsableActor::StaticRegisterNativesASUsableActor()
	{
	}
	UClass* Z_Construct_UClass_ASUsableActor_NoRegister()
	{
		return ASUsableActor::StaticClass();
	}
	UClass* Z_Construct_UClass_ASUsableActor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASUsableActor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_MeshComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeshComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MeshComp, ASUsableActor), 0x00200800000a0009, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASUsableActor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SUsableActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SUsableActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_MeshComp, TEXT("Category"), TEXT("Mesh"));
				MetaData->SetValue(NewProp_MeshComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MeshComp, TEXT("ModuleRelativePath"), TEXT("Public/Items/SUsableActor.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASUsableActor, 1729384772);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASUsableActor(Z_Construct_UClass_ASUsableActor, &ASUsableActor::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASUsableActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASUsableActor);
	static FName NAME_ASBombActor_SimulateExplosion = FName(TEXT("SimulateExplosion"));
	void ASBombActor::SimulateExplosion()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASBombActor_SimulateExplosion),NULL);
	}
	static FName NAME_ASBombActor_SimulateFuzeFX = FName(TEXT("SimulateFuzeFX"));
	void ASBombActor::SimulateFuzeFX()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASBombActor_SimulateFuzeFX),NULL);
	}
	void ASBombActor::StaticRegisterNativesASBombActor()
	{
		UClass* Class = ASBombActor::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "SimulateExplosion", (Native)&ASBombActor::execSimulateExplosion },
			{ "SimulateFuzeFX", (Native)&ASBombActor::execSimulateFuzeFX },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 2);
	}
	UFunction* Z_Construct_UFunction_ASBombActor_SimulateExplosion()
	{
		UObject* Outer=Z_Construct_UClass_ASBombActor();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SimulateExplosion"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00084CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASBombActor_SimulateFuzeFX()
	{
		UObject* Outer=Z_Construct_UClass_ASBombActor();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SimulateFuzeFX"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00084CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASBombActor_NoRegister()
	{
		return ASBombActor::StaticClass();
	}
	UClass* Z_Construct_UClass_ASBombActor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASUsableActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASBombActor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ASBombActor_SimulateExplosion());
				OuterClass->LinkChild(Z_Construct_UFunction_ASBombActor_SimulateFuzeFX());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_DamageType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DamageType"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(DamageType, ASBombActor), 0x0024080000010001, Z_Construct_UClass_UDamageType_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_ExplosionRadius = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionRadius"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ExplosionRadius, ASBombActor), 0x0020080000010001);
				UProperty* NewProp_ExplosionDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ExplosionDamage, ASBombActor), 0x0020080000010001);
				UProperty* NewProp_MaxFuzeTime = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxFuzeTime"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxFuzeTime, ASBombActor), 0x0020080000010001);
				UProperty* NewProp_FuzeSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FuzeSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FuzeSound, ASBombActor), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_ExplosionSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ExplosionSound, ASBombActor), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_FuzeFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FuzeFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FuzeFX, ASBombActor), 0x0020080000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_ExplosionFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ExplosionFX, ASBombActor), 0x0020080000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_AudioComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AudioComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(AudioComp, ASBombActor), 0x00200800000b0009, Z_Construct_UClass_UAudioComponent_NoRegister());
				UProperty* NewProp_FuzePCS = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FuzePCS"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FuzePCS, ASBombActor), 0x00200800000b0009, Z_Construct_UClass_UParticleSystemComponent_NoRegister());
				UProperty* NewProp_ExplosionPCS = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExplosionPCS"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ExplosionPCS, ASBombActor), 0x00200800000b0009, Z_Construct_UClass_UParticleSystemComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBombActor_SimulateExplosion(), "SimulateExplosion"); // 2066484307
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASBombActor_SimulateFuzeFX(), "SimulateFuzeFX"); // 2347016702
				static TCppClassTypeInfo<TCppClassTypeTraits<ASBombActor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SBombActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_DamageType, TEXT("Category"), TEXT("Bomb|Settings"));
				MetaData->SetValue(NewProp_DamageType, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_ExplosionRadius, TEXT("Category"), TEXT("Bomb|Settings"));
				MetaData->SetValue(NewProp_ExplosionRadius, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_ExplosionDamage, TEXT("Category"), TEXT("Bomb|Settings"));
				MetaData->SetValue(NewProp_ExplosionDamage, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_MaxFuzeTime, TEXT("Category"), TEXT("Bomb|Settings"));
				MetaData->SetValue(NewProp_MaxFuzeTime, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_MaxFuzeTime, TEXT("ToolTip"), TEXT("Initial time on the fuze"));
				MetaData->SetValue(NewProp_FuzeSound, TEXT("Category"), TEXT("Bomb|Effects"));
				MetaData->SetValue(NewProp_FuzeSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_ExplosionSound, TEXT("Category"), TEXT("Bomb|Effects"));
				MetaData->SetValue(NewProp_ExplosionSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_FuzeFX, TEXT("Category"), TEXT("Bomb|Effects"));
				MetaData->SetValue(NewProp_FuzeFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_ExplosionFX, TEXT("Category"), TEXT("Bomb|Effects"));
				MetaData->SetValue(NewProp_ExplosionFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_AudioComp, TEXT("Category"), TEXT("SBombActor"));
				MetaData->SetValue(NewProp_AudioComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_AudioComp, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_FuzePCS, TEXT("Category"), TEXT("SBombActor"));
				MetaData->SetValue(NewProp_FuzePCS, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_FuzePCS, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
				MetaData->SetValue(NewProp_ExplosionPCS, TEXT("Category"), TEXT("SBombActor"));
				MetaData->SetValue(NewProp_ExplosionPCS, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_ExplosionPCS, TEXT("ModuleRelativePath"), TEXT("Public/Items/SBombActor.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASBombActor, 565372241);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASBombActor(Z_Construct_UClass_ASBombActor, &ASBombActor::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASBombActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASBombActor);
	void ASPickupActor::StaticRegisterNativesASPickupActor()
	{
		UClass* Class = ASPickupActor::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "OnRep_IsActive", (Native)&ASPickupActor::execOnRep_IsActive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_ASPickupActor_OnRep_IsActive()
	{
		UObject* Outer=Z_Construct_UClass_ASPickupActor();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_IsActive"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00040401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASPickupActor_NoRegister()
	{
		return ASPickupActor::StaticClass();
	}
	UClass* Z_Construct_UClass_ASPickupActor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASUsableActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASPickupActor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASPickupActor_OnRep_IsActive());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_RespawnDelayRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RespawnDelayRange"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RespawnDelayRange, ASPickupActor), 0x0010000000010001);
				UProperty* NewProp_RespawnDelay = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("RespawnDelay"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(RespawnDelay, ASPickupActor), 0x0010000000010001);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bAllowRespawn, ASPickupActor, bool);
				UProperty* NewProp_bAllowRespawn = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bAllowRespawn"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bAllowRespawn, ASPickupActor), 0x0010000000010001, CPP_BOOL_PROPERTY_BITMASK(bAllowRespawn, ASPickupActor), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bStartActive, ASPickupActor, bool);
				UProperty* NewProp_bStartActive = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bStartActive"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bStartActive, ASPickupActor), 0x0010000000010001, CPP_BOOL_PROPERTY_BITMASK(bStartActive, ASPickupActor), sizeof(bool), true);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsActive, ASPickupActor, bool);
				UProperty* NewProp_bIsActive = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsActive"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsActive, ASPickupActor), 0x0020080100002020, CPP_BOOL_PROPERTY_BITMASK(bIsActive, ASPickupActor), sizeof(bool), true);
				NewProp_bIsActive->RepNotifyFunc = FName(TEXT("OnRep_IsActive"));
				UProperty* NewProp_PickupSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PickupSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(PickupSound, ASPickupActor), 0x0040000000010001, Z_Construct_UClass_USoundCue_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASPickupActor_OnRep_IsActive(), "OnRep_IsActive"); // 1552518689
				static TCppClassTypeInfo<TCppClassTypeTraits<ASPickupActor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SPickupActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_RespawnDelayRange, TEXT("Category"), TEXT("Pickup"));
				MetaData->SetValue(NewProp_RespawnDelayRange, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(NewProp_RespawnDelayRange, TEXT("ToolTip"), TEXT("Extra delay randomly applied on the respawn timer"));
				MetaData->SetValue(NewProp_RespawnDelay, TEXT("Category"), TEXT("Pickup"));
				MetaData->SetValue(NewProp_RespawnDelay, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(NewProp_bAllowRespawn, TEXT("Category"), TEXT("Pickup"));
				MetaData->SetValue(NewProp_bAllowRespawn, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(NewProp_bAllowRespawn, TEXT("ToolTip"), TEXT("Will this item ever respawn"));
				MetaData->SetValue(NewProp_bStartActive, TEXT("Category"), TEXT("Pickup"));
				MetaData->SetValue(NewProp_bStartActive, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(NewProp_bStartActive, TEXT("ToolTip"), TEXT("Immediately spawn on begin play"));
				MetaData->SetValue(NewProp_bIsActive, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
				MetaData->SetValue(NewProp_PickupSound, TEXT("Category"), TEXT("Sound"));
				MetaData->SetValue(NewProp_PickupSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SPickupActor.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASPickupActor, 322620669);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASPickupActor(Z_Construct_UClass_ASPickupActor, &ASPickupActor::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASPickupActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASPickupActor);
	void ASConsumableActor::StaticRegisterNativesASConsumableActor()
	{
	}
	UClass* Z_Construct_UClass_ASConsumableActor_NoRegister()
	{
		return ASConsumableActor::StaticClass();
	}
	UClass* Z_Construct_UClass_ASConsumableActor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASPickupActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASConsumableActor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_Nutrition = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Nutrition"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(Nutrition, ASConsumableActor), 0x0020080000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASConsumableActor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SConsumableActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SConsumableActor.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(OuterClass, TEXT("ToolTip"), TEXT("Base class for consumable (food) items. Consumable by players to restore energy"));
				MetaData->SetValue(NewProp_Nutrition, TEXT("Category"), TEXT("Consumable"));
				MetaData->SetValue(NewProp_Nutrition, TEXT("ModuleRelativePath"), TEXT("Public/Items/SConsumableActor.h"));
				MetaData->SetValue(NewProp_Nutrition, TEXT("ToolTip"), TEXT("Amount of hitpoints restored and hunger reduced when consumed."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASConsumableActor, 1742285768);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASConsumableActor(Z_Construct_UClass_ASConsumableActor, &ASConsumableActor::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASConsumableActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASConsumableActor);
	void ASWeaponPickup::StaticRegisterNativesASWeaponPickup()
	{
	}
	UClass* Z_Construct_UClass_ASWeaponPickup_NoRegister()
	{
		return ASWeaponPickup::StaticClass();
	}
	UClass* Z_Construct_UClass_ASWeaponPickup()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASPickupActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASWeaponPickup::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_WeaponClass = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WeaponClass"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(WeaponClass, ASWeaponPickup), 0x0014000000010001, Z_Construct_UClass_ASWeapon_NoRegister(), UClass::StaticClass());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASWeaponPickup> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SWeaponPickup.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponPickup.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_WeaponClass, TEXT("Category"), TEXT("WeaponClass"));
				MetaData->SetValue(NewProp_WeaponClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponPickup.h"));
				MetaData->SetValue(NewProp_WeaponClass, TEXT("ToolTip"), TEXT("Class to add to inventory when picked up"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASWeaponPickup, 3991881779);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASWeaponPickup(Z_Construct_UClass_ASWeaponPickup, &ASWeaponPickup::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASWeaponPickup"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASWeaponPickup);
static UEnum* EWeaponState_StaticEnum()
{
	extern SURVIVALGAME_API class UPackage* Z_Construct_UPackage__Script_SurvivalGame();
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		extern SURVIVALGAME_API class UEnum* Z_Construct_UEnum_SurvivalGame_EWeaponState();
		Singleton = GetStaticEnum(Z_Construct_UEnum_SurvivalGame_EWeaponState, Z_Construct_UPackage__Script_SurvivalGame(), TEXT("EWeaponState"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EWeaponState(EWeaponState_StaticEnum, TEXT("/Script/SurvivalGame"), TEXT("EWeaponState"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_SurvivalGame_EWeaponState()
	{
		UPackage* Outer=Z_Construct_UPackage__Script_SurvivalGame();
		extern uint32 Get_Z_Construct_UEnum_SurvivalGame_EWeaponState_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EWeaponState"), 0, Get_Z_Construct_UEnum_SurvivalGame_EWeaponState_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EWeaponState"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EWeaponState::Idle"), 0);
			EnumNames.Emplace(TEXT("EWeaponState::Firing"), 1);
			EnumNames.Emplace(TEXT("EWeaponState::Equipping"), 2);
			EnumNames.Emplace(TEXT("EWeaponState::Reloading"), 3);
			EnumNames.Emplace(TEXT("EWeaponState::EWeaponState_MAX"), 4);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EWeaponState");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_SurvivalGame_EWeaponState_CRC() { return 1318564160U; }
	static FName NAME_ASWeapon_ClientStartReload = FName(TEXT("ClientStartReload"));
	void ASWeapon::ClientStartReload()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ClientStartReload),NULL);
	}
	static FName NAME_ASWeapon_ServerHandleFiring = FName(TEXT("ServerHandleFiring"));
	void ASWeapon::ServerHandleFiring()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ServerHandleFiring),NULL);
	}
	static FName NAME_ASWeapon_ServerStartFire = FName(TEXT("ServerStartFire"));
	void ASWeapon::ServerStartFire()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ServerStartFire),NULL);
	}
	static FName NAME_ASWeapon_ServerStartReload = FName(TEXT("ServerStartReload"));
	void ASWeapon::ServerStartReload()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ServerStartReload),NULL);
	}
	static FName NAME_ASWeapon_ServerStopFire = FName(TEXT("ServerStopFire"));
	void ASWeapon::ServerStopFire()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ServerStopFire),NULL);
	}
	static FName NAME_ASWeapon_ServerStopReload = FName(TEXT("ServerStopReload"));
	void ASWeapon::ServerStopReload()
	{
		ProcessEvent(FindFunctionChecked(NAME_ASWeapon_ServerStopReload),NULL);
	}
	void ASWeapon::StaticRegisterNativesASWeapon()
	{
		UClass* Class = ASWeapon::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "ClientStartReload", (Native)&ASWeapon::execClientStartReload },
			{ "GetCurrentAmmo", (Native)&ASWeapon::execGetCurrentAmmo },
			{ "GetCurrentAmmoInClip", (Native)&ASWeapon::execGetCurrentAmmoInClip },
			{ "GetMaxAmmo", (Native)&ASWeapon::execGetMaxAmmo },
			{ "GetMaxAmmoPerClip", (Native)&ASWeapon::execGetMaxAmmoPerClip },
			{ "GetPawnOwner", (Native)&ASWeapon::execGetPawnOwner },
			{ "GetWeaponMesh", (Native)&ASWeapon::execGetWeaponMesh },
			{ "OnRep_BurstCounter", (Native)&ASWeapon::execOnRep_BurstCounter },
			{ "OnRep_MyPawn", (Native)&ASWeapon::execOnRep_MyPawn },
			{ "OnRep_Reload", (Native)&ASWeapon::execOnRep_Reload },
			{ "ServerHandleFiring", (Native)&ASWeapon::execServerHandleFiring },
			{ "ServerStartFire", (Native)&ASWeapon::execServerStartFire },
			{ "ServerStartReload", (Native)&ASWeapon::execServerStartReload },
			{ "ServerStopFire", (Native)&ASWeapon::execServerStopFire },
			{ "ServerStopReload", (Native)&ASWeapon::execServerStopReload },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 15);
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ClientStartReload()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ClientStartReload"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x01080CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Trigger reload from server"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetCurrentAmmo()
	{
		struct SWeapon_eventGetCurrentAmmo_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetCurrentAmmo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetCurrentAmmo_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetCurrentAmmo_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Ammo"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetCurrentAmmoInClip()
	{
		struct SWeapon_eventGetCurrentAmmoInClip_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetCurrentAmmoInClip"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetCurrentAmmoInClip_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetCurrentAmmoInClip_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Ammo"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetMaxAmmo()
	{
		struct SWeapon_eventGetMaxAmmo_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetMaxAmmo"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetMaxAmmo_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetMaxAmmo_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Ammo"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetMaxAmmoPerClip()
	{
		struct SWeapon_eventGetMaxAmmoPerClip_Parms
		{
			int32 ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetMaxAmmoPerClip"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetMaxAmmoPerClip_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetMaxAmmoPerClip_Parms), 0x0010000000000580);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Ammo"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetPawnOwner()
	{
		struct SWeapon_eventGetPawnOwner_Parms
		{
			ASCharacter* ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetPawnOwner"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetPawnOwner_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetPawnOwner_Parms), 0x0010000000000580, Z_Construct_UClass_ASCharacter_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Get pawn owner"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_GetWeaponMesh()
	{
		struct SWeapon_eventGetWeaponMesh_Parms
		{
			USkeletalMeshComponent* ReturnValue;
		};
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("GetWeaponMesh"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x54020401, 65535, sizeof(SWeapon_eventGetWeaponMesh_Parms));
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReturnValue, SWeapon_eventGetWeaponMesh_Parms), 0x0010000000080588, Z_Construct_UClass_USkeletalMeshComponent_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Weapon"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("get weapon mesh (needs pawn owner to determine variant)"));
			MetaData->SetValue(NewProp_ReturnValue, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_OnRep_BurstCounter()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_BurstCounter"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00040401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_OnRep_MyPawn()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_MyPawn"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00080401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_OnRep_Reload()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_Reload"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00080401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ServerHandleFiring()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerHandleFiring"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80240CC1, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ServerStartFire()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerStartFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80240CC1, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ServerStartReload()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerStartReload"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ServerStopFire()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerStopFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80240CC1, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeapon_ServerStopReload()
	{
		UObject* Outer=Z_Construct_UClass_ASWeapon();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerStopReload"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASWeapon_NoRegister()
	{
		return ASWeapon::StaticClass();
	}
	UClass* Z_Construct_UClass_ASWeapon()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASWeapon::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ClientStartReload());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetCurrentAmmo());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetCurrentAmmoInClip());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetMaxAmmo());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetMaxAmmoPerClip());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetPawnOwner());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_GetWeaponMesh());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_OnRep_BurstCounter());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_OnRep_MyPawn());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_OnRep_Reload());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ServerHandleFiring());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ServerStartFire());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ServerStartReload());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ServerStopFire());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeapon_ServerStopReload());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_ReloadAnim = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ReloadAnim"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReloadAnim, ASWeapon), 0x0020080000010001, Z_Construct_UClass_UAnimMontage_NoRegister());
				UProperty* NewProp_ReloadSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ReloadSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ReloadSound, ASWeapon), 0x0020080000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_MaxAmmoPerClip = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxAmmoPerClip"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(MaxAmmoPerClip, ASWeapon), 0x0020080000010001);
				UProperty* NewProp_MaxAmmo = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxAmmo"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(MaxAmmo, ASWeapon), 0x0020080000010001);
				UProperty* NewProp_StartAmmo = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("StartAmmo"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(StartAmmo, ASWeapon), 0x0020080000010001);
				UProperty* NewProp_CurrentAmmoInClip = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentAmmoInClip"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(CurrentAmmoInClip, ASWeapon), 0x0020080000002020);
				UProperty* NewProp_CurrentAmmo = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentAmmo"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(CurrentAmmo, ASWeapon), 0x0020080000002020);
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bPendingReload, ASWeapon, bool);
				UProperty* NewProp_bPendingReload = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bPendingReload"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bPendingReload, ASWeapon), 0x0020080100002020, CPP_BOOL_PROPERTY_BITMASK(bPendingReload, ASWeapon), sizeof(bool), true);
				NewProp_bPendingReload->RepNotifyFunc = FName(TEXT("OnRep_Reload"));
				UProperty* NewProp_NoEquipAnimDuration = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NoEquipAnimDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(NoEquipAnimDuration, ASWeapon), 0x0020080000010001);
				UProperty* NewProp_NoAnimReloadDuration = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("NoAnimReloadDuration"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(NoAnimReloadDuration, ASWeapon), 0x0020080000010001);
				UProperty* NewProp_OutOfAmmoSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("OutOfAmmoSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(OutOfAmmoSound, ASWeapon), 0x0040000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_BurstCounter = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BurstCounter"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(BurstCounter, ASWeapon), 0x0040000100002020);
				NewProp_BurstCounter->RepNotifyFunc = FName(TEXT("OnRep_BurstCounter"));
				UProperty* NewProp_MuzzleAttachPoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MuzzleAttachPoint"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(MuzzleAttachPoint, ASWeapon), 0x0040000000010001);
				UProperty* NewProp_MuzzlePSC = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MuzzlePSC"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MuzzlePSC, ASWeapon), 0x0040000000082008, Z_Construct_UClass_UParticleSystemComponent_NoRegister());
				UProperty* NewProp_FireAnim = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FireAnim"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FireAnim, ASWeapon), 0x0040000000010001, Z_Construct_UClass_UAnimMontage_NoRegister());
				UProperty* NewProp_EquipAnim = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EquipAnim"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(EquipAnim, ASWeapon), 0x0040000000010001, Z_Construct_UClass_UAnimMontage_NoRegister());
				UProperty* NewProp_MuzzleFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MuzzleFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MuzzleFX, ASWeapon), 0x0040000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_EquipSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EquipSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(EquipSound, ASWeapon), 0x0040000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_FireSound = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FireSound"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(FireSound, ASWeapon), 0x0040000000010001, Z_Construct_UClass_USoundCue_NoRegister());
				UProperty* NewProp_WeaponPickupClass = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WeaponPickupClass"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(WeaponPickupClass, ASWeapon), 0x0014000000010001, Z_Construct_UClass_ASWeaponPickup_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_Mesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Mesh"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(Mesh, ASWeapon), 0x00200800000b0009, Z_Construct_UClass_USkeletalMeshComponent_NoRegister());
				UProperty* NewProp_MyPawn = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MyPawn"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MyPawn, ASWeapon), 0x0020080100002020, Z_Construct_UClass_ASCharacter_NoRegister());
				NewProp_MyPawn->RepNotifyFunc = FName(TEXT("OnRep_MyPawn"));
				UProperty* NewProp_StorageSlot = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("StorageSlot"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(StorageSlot, ASWeapon), 0x0020080000010001, Z_Construct_UEnum_SurvivalGame_EInventorySlot());
				UProperty* NewProp_StorageSlot_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_StorageSlot, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				UProperty* NewProp_ShotsPerMinute = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ShotsPerMinute"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ShotsPerMinute, ASWeapon), 0x0040000000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ClientStartReload(), "ClientStartReload"); // 482591551
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetCurrentAmmo(), "GetCurrentAmmo"); // 3397695441
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetCurrentAmmoInClip(), "GetCurrentAmmoInClip"); // 1618806257
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetMaxAmmo(), "GetMaxAmmo"); // 3886978433
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetMaxAmmoPerClip(), "GetMaxAmmoPerClip"); // 2441459195
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetPawnOwner(), "GetPawnOwner"); // 238663446
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_GetWeaponMesh(), "GetWeaponMesh"); // 1091371127
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_OnRep_BurstCounter(), "OnRep_BurstCounter"); // 1428496260
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_OnRep_MyPawn(), "OnRep_MyPawn"); // 658996800
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_OnRep_Reload(), "OnRep_Reload"); // 2624317756
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ServerHandleFiring(), "ServerHandleFiring"); // 1810267430
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ServerStartFire(), "ServerStartFire"); // 2214365702
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ServerStartReload(), "ServerStartReload"); // 3623539483
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ServerStopFire(), "ServerStopFire"); // 1432491238
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeapon_ServerStopReload(), "ServerStopReload"); // 3964010808
				static TCppClassTypeInfo<TCppClassTypeTraits<ASWeapon> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SWeapon.h"));
				MetaData->SetValue(OuterClass, TEXT("IsBlueprintBase"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_ReloadAnim, TEXT("Category"), TEXT("Animation"));
				MetaData->SetValue(NewProp_ReloadAnim, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_ReloadSound, TEXT("Category"), TEXT("Sounds"));
				MetaData->SetValue(NewProp_ReloadSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MaxAmmoPerClip, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_MaxAmmoPerClip, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MaxAmmo, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_MaxAmmo, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_StartAmmo, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_StartAmmo, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_StartAmmo, TEXT("ToolTip"), TEXT("Weapon ammo on spawn"));
				MetaData->SetValue(NewProp_CurrentAmmoInClip, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_CurrentAmmo, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_bPendingReload, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_NoEquipAnimDuration, TEXT("Category"), TEXT("Animation"));
				MetaData->SetValue(NewProp_NoEquipAnimDuration, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_NoEquipAnimDuration, TEXT("ToolTip"), TEXT("Time to assign on equip when no animation is found"));
				MetaData->SetValue(NewProp_NoAnimReloadDuration, TEXT("Category"), TEXT("Animation"));
				MetaData->SetValue(NewProp_NoAnimReloadDuration, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_NoAnimReloadDuration, TEXT("ToolTip"), TEXT("Time to assign on reload when no animation is found"));
				MetaData->SetValue(NewProp_OutOfAmmoSound, TEXT("Category"), TEXT("Sounds"));
				MetaData->SetValue(NewProp_OutOfAmmoSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_BurstCounter, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MuzzleAttachPoint, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_MuzzleAttachPoint, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MuzzlePSC, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MuzzlePSC, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_FireAnim, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_FireAnim, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_EquipAnim, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_EquipAnim, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MuzzleFX, TEXT("Category"), TEXT("SWeapon"));
				MetaData->SetValue(NewProp_MuzzleFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_EquipSound, TEXT("Category"), TEXT("Sounds"));
				MetaData->SetValue(NewProp_EquipSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_FireSound, TEXT("Category"), TEXT("Sounds"));
				MetaData->SetValue(NewProp_FireSound, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_WeaponPickupClass, TEXT("Category"), TEXT("Weapon"));
				MetaData->SetValue(NewProp_WeaponPickupClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_WeaponPickupClass, TEXT("ToolTip"), TEXT("The class to spawn in the level when dropped"));
				MetaData->SetValue(NewProp_Mesh, TEXT("Category"), TEXT("Mesh"));
				MetaData->SetValue(NewProp_Mesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_Mesh, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_Mesh, TEXT("ToolTip"), TEXT("weapon mesh: 3rd person view"));
				MetaData->SetValue(NewProp_MyPawn, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_MyPawn, TEXT("ToolTip"), TEXT("pawn owner"));
				MetaData->SetValue(NewProp_StorageSlot, TEXT("Category"), TEXT("Weapon"));
				MetaData->SetValue(NewProp_StorageSlot, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
				MetaData->SetValue(NewProp_StorageSlot, TEXT("ToolTip"), TEXT("The character socket to store this item at."));
				MetaData->SetValue(NewProp_ShotsPerMinute, TEXT("Category"), TEXT("Weapon"));
				MetaData->SetValue(NewProp_ShotsPerMinute, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeapon.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASWeapon, 4027260360);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASWeapon(Z_Construct_UClass_ASWeapon, &ASWeapon::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASWeapon);
	void ASFlashlight::StaticRegisterNativesASFlashlight()
	{
		UClass* Class = ASFlashlight::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "OnRep_IsActive", (Native)&ASFlashlight::execOnRep_IsActive },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 1);
	}
	UFunction* Z_Construct_UFunction_ASFlashlight_OnRep_IsActive()
	{
		UObject* Outer=Z_Construct_UClass_ASFlashlight();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_IsActive"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASFlashlight_NoRegister()
	{
		return ASFlashlight::StaticClass();
	}
	UClass* Z_Construct_UClass_ASFlashlight()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASWeapon();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASFlashlight::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASFlashlight_OnRep_IsActive());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_SpotLightComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpotLightComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(SpotLightComp, ASFlashlight), 0x00100000000b0009, Z_Construct_UClass_USpotLightComponent_NoRegister());
				UProperty* NewProp_LightConeComp = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("LightConeComp"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(LightConeComp, ASFlashlight), 0x00100000000b0009, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(bIsActive, ASFlashlight, bool);
				UProperty* NewProp_bIsActive = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("bIsActive"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bIsActive, ASFlashlight), 0x0010000100002020, CPP_BOOL_PROPERTY_BITMASK(bIsActive, ASFlashlight), sizeof(bool), true);
				NewProp_bIsActive->RepNotifyFunc = FName(TEXT("OnRep_IsActive"));
				UProperty* NewProp_LightAttachPoint = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("LightAttachPoint"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(LightAttachPoint, ASFlashlight), 0x0010000000010001);
				UProperty* NewProp_MaxEmissiveIntensity = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MaxEmissiveIntensity"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MaxEmissiveIntensity, ASFlashlight), 0x0040000000010001);
				UProperty* NewProp_EmissiveParamName = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EmissiveParamName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(EmissiveParamName, ASFlashlight), 0x0040000000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASFlashlight_OnRep_IsActive(), "OnRep_IsActive"); // 1949270551
				static TCppClassTypeInfo<TCppClassTypeTraits<ASFlashlight> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SFlashlight.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_SpotLightComp, TEXT("Category"), TEXT("SFlashlight"));
				MetaData->SetValue(NewProp_SpotLightComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_SpotLightComp, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(NewProp_LightConeComp, TEXT("Category"), TEXT("SFlashlight"));
				MetaData->SetValue(NewProp_LightConeComp, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_LightConeComp, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(NewProp_bIsActive, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(NewProp_LightAttachPoint, TEXT("Category"), TEXT("SFlashlight"));
				MetaData->SetValue(NewProp_LightAttachPoint, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(NewProp_MaxEmissiveIntensity, TEXT("Category"), TEXT("Material"));
				MetaData->SetValue(NewProp_MaxEmissiveIntensity, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
				MetaData->SetValue(NewProp_EmissiveParamName, TEXT("Category"), TEXT("Material"));
				MetaData->SetValue(NewProp_EmissiveParamName, TEXT("ModuleRelativePath"), TEXT("Public/Items/SFlashlight.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASFlashlight, 2174596644);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASFlashlight(Z_Construct_UClass_ASFlashlight, &ASFlashlight::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASFlashlight"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASFlashlight);
	static FName NAME_ASWeaponInstant_ServerNotifyHit = FName(TEXT("ServerNotifyHit"));
	void ASWeaponInstant::ServerNotifyHit(const FHitResult Impact, FVector_NetQuantizeNormal ShootDir)
	{
		SWeaponInstant_eventServerNotifyHit_Parms Parms;
		Parms.Impact=Impact;
		Parms.ShootDir=ShootDir;
		ProcessEvent(FindFunctionChecked(NAME_ASWeaponInstant_ServerNotifyHit),&Parms);
	}
	static FName NAME_ASWeaponInstant_ServerNotifyMiss = FName(TEXT("ServerNotifyMiss"));
	void ASWeaponInstant::ServerNotifyMiss(FVector_NetQuantizeNormal ShootDir)
	{
		SWeaponInstant_eventServerNotifyMiss_Parms Parms;
		Parms.ShootDir=ShootDir;
		ProcessEvent(FindFunctionChecked(NAME_ASWeaponInstant_ServerNotifyMiss),&Parms);
	}
	void ASWeaponInstant::StaticRegisterNativesASWeaponInstant()
	{
		UClass* Class = ASWeaponInstant::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "OnRep_HitLocation", (Native)&ASWeaponInstant::execOnRep_HitLocation },
			{ "ServerNotifyHit", (Native)&ASWeaponInstant::execServerNotifyHit },
			{ "ServerNotifyMiss", (Native)&ASWeaponInstant::execServerNotifyMiss },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, 3);
	}
	UFunction* Z_Construct_UFunction_ASWeaponInstant_OnRep_HitLocation()
	{
		UObject* Outer=Z_Construct_UClass_ASWeaponInstant();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnRep_HitLocation"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x00080401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeaponInstant_ServerNotifyHit()
	{
		UObject* Outer=Z_Construct_UClass_ASWeaponInstant();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerNotifyHit"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535, sizeof(SWeaponInstant_eventServerNotifyHit_Parms));
			UProperty* NewProp_ShootDir = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ShootDir"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(ShootDir, SWeaponInstant_eventServerNotifyHit_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector_NetQuantizeNormal());
			UProperty* NewProp_Impact = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("Impact"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(Impact, SWeaponInstant_eventServerNotifyHit_Parms), 0x0010008000000082, Z_Construct_UScriptStruct_FHitResult());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
			MetaData->SetValue(NewProp_Impact, TEXT("NativeConst"), TEXT(""));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ASWeaponInstant_ServerNotifyMiss()
	{
		UObject* Outer=Z_Construct_UClass_ASWeaponInstant();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("ServerNotifyMiss"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), NULL, 0x80280CC0, 65535, sizeof(SWeaponInstant_eventServerNotifyMiss_Parms));
			UProperty* NewProp_ShootDir = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ShootDir"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(ShootDir, SWeaponInstant_eventServerNotifyMiss_Parms), 0x0010000000000080, Z_Construct_UScriptStruct_FVector_NetQuantizeNormal());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASWeaponInstant_NoRegister()
	{
		return ASWeaponInstant::StaticClass();
	}
	UClass* Z_Construct_UClass_ASWeaponInstant()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ASWeapon();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASWeaponInstant::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900081;

				OuterClass->LinkChild(Z_Construct_UFunction_ASWeaponInstant_OnRep_HitLocation());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeaponInstant_ServerNotifyHit());
				OuterClass->LinkChild(Z_Construct_UFunction_ASWeaponInstant_ServerNotifyMiss());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_ClientSideHitLeeway = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ClientSideHitLeeway"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(ClientSideHitLeeway, ASWeaponInstant), 0x0020080000010001);
				UProperty* NewProp_AllowedViewDotHitDir = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("AllowedViewDotHitDir"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(AllowedViewDotHitDir, ASWeaponInstant), 0x0020080000010001);
				UProperty* NewProp_WeaponRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WeaponRange"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(WeaponRange, ASWeaponInstant), 0x0020080000010001);
				UProperty* NewProp_DamageType = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("DamageType"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(DamageType, ASWeaponInstant), 0x0024080000010001, Z_Construct_UClass_UDamageType_NoRegister(), UClass::StaticClass());
				UProperty* NewProp_HitDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HitDamage"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(HitDamage, ASWeaponInstant), 0x0020080000010001);
				UProperty* NewProp_HitImpactNotify = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("HitImpactNotify"), RF_Public|RF_Transient|RF_MarkAsNative) UStructProperty(CPP_PROPERTY_BASE(HitImpactNotify, ASWeaponInstant), 0x0020080100002020, Z_Construct_UScriptStruct_FVector());
				NewProp_HitImpactNotify->RepNotifyFunc = FName(TEXT("OnRep_HitLocation"));
				UProperty* NewProp_TracerRoundInterval = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TracerRoundInterval"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(TracerRoundInterval, ASWeaponInstant), 0x0040000000010001);
				UProperty* NewProp_MinimumProjectileSpawnDistance = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MinimumProjectileSpawnDistance"), RF_Public|RF_Transient|RF_MarkAsNative) UFloatProperty(CPP_PROPERTY_BASE(MinimumProjectileSpawnDistance, ASWeaponInstant), 0x0040000000010001);
				UProperty* NewProp_TracerFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TracerFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TracerFX, ASWeaponInstant), 0x0040000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_TrailFX = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TrailFX"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(TrailFX, ASWeaponInstant), 0x0040000000010001, Z_Construct_UClass_UParticleSystem_NoRegister());
				UProperty* NewProp_TrailTargetParam = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TrailTargetParam"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(TrailTargetParam, ASWeaponInstant), 0x0040000000010001);
				UProperty* NewProp_ImpactTemplate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ImpactTemplate"), RF_Public|RF_Transient|RF_MarkAsNative) UClassProperty(CPP_PROPERTY_BASE(ImpactTemplate, ASWeaponInstant), 0x0044000000010001, Z_Construct_UClass_ASImpactEffect_NoRegister(), UClass::StaticClass());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeaponInstant_OnRep_HitLocation(), "OnRep_HitLocation"); // 3408725490
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeaponInstant_ServerNotifyHit(), "ServerNotifyHit"); // 2442338774
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_ASWeaponInstant_ServerNotifyMiss(), "ServerNotifyMiss"); // 451542511
				static TCppClassTypeInfo<TCppClassTypeTraits<ASWeaponInstant> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Items/SWeaponInstant.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_ClientSideHitLeeway, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_ClientSideHitLeeway, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_ClientSideHitLeeway, TEXT("ToolTip"), TEXT("Hit verification: scale for bounding box of hit actor"));
				MetaData->SetValue(NewProp_AllowedViewDotHitDir, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_AllowedViewDotHitDir, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_AllowedViewDotHitDir, TEXT("ToolTip"), TEXT("Hit verification: threshold for dot product between view direction and hit direction"));
				MetaData->SetValue(NewProp_WeaponRange, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_WeaponRange, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_DamageType, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_DamageType, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_HitDamage, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_HitDamage, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_HitImpactNotify, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_TracerRoundInterval, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_TracerRoundInterval, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_MinimumProjectileSpawnDistance, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_MinimumProjectileSpawnDistance, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_MinimumProjectileSpawnDistance, TEXT("ToolTip"), TEXT("Minimum firing distance before spawning tracers or trails."));
				MetaData->SetValue(NewProp_TracerFX, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_TracerFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_TrailFX, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_TrailFX, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_TrailTargetParam, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_TrailTargetParam, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_ImpactTemplate, TEXT("Category"), TEXT("SWeaponInstant"));
				MetaData->SetValue(NewProp_ImpactTemplate, TEXT("ModuleRelativePath"), TEXT("Public/Items/SWeaponInstant.h"));
				MetaData->SetValue(NewProp_ImpactTemplate, TEXT("ToolTip"), TEXT("Particle FX played when a surface is hit."));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASWeaponInstant, 4209198741);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASWeaponInstant(Z_Construct_UClass_ASWeaponInstant, &ASWeaponInstant::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASWeaponInstant"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASWeaponInstant);
	void ASZombieAIController::StaticRegisterNativesASZombieAIController()
	{
	}
	UClass* Z_Construct_UClass_ASZombieAIController_NoRegister()
	{
		return ASZombieAIController::StaticClass();
	}
	UClass* Z_Construct_UClass_ASZombieAIController()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AAIController();
			Z_Construct_UPackage__Script_SurvivalGame();
			OuterClass = ASZombieAIController::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900280;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_BotTypeKeyName = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BotTypeKeyName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(BotTypeKeyName, ASZombieAIController), 0x0040000000010001);
				UProperty* NewProp_CurrentWaypointKeyName = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CurrentWaypointKeyName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(CurrentWaypointKeyName, ASZombieAIController), 0x0040000000010001);
				UProperty* NewProp_PatrolLocationKeyName = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("PatrolLocationKeyName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(PatrolLocationKeyName, ASZombieAIController), 0x0040000000010001);
				UProperty* NewProp_TargetEnemyKeyName = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TargetEnemyKeyName"), RF_Public|RF_Transient|RF_MarkAsNative) UNameProperty(CPP_PROPERTY_BASE(TargetEnemyKeyName, ASZombieAIController), 0x0040000000010001);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				static TCppClassTypeInfo<TCppClassTypeTraits<ASZombieAIController> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("AI/SZombieAIController.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieAIController.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_BotTypeKeyName, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_BotTypeKeyName, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieAIController.h"));
				MetaData->SetValue(NewProp_CurrentWaypointKeyName, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_CurrentWaypointKeyName, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieAIController.h"));
				MetaData->SetValue(NewProp_PatrolLocationKeyName, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_PatrolLocationKeyName, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieAIController.h"));
				MetaData->SetValue(NewProp_TargetEnemyKeyName, TEXT("Category"), TEXT("AI"));
				MetaData->SetValue(NewProp_TargetEnemyKeyName, TEXT("ModuleRelativePath"), TEXT("Public/AI/SZombieAIController.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASZombieAIController, 2142347898);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASZombieAIController(Z_Construct_UClass_ASZombieAIController, &ASZombieAIController::StaticClass, TEXT("/Script/SurvivalGame"), TEXT("ASZombieAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASZombieAIController);
	UPackage* Z_Construct_UPackage__Script_SurvivalGame()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), nullptr, FName(TEXT("/Script/SurvivalGame")), false, false));
			ReturnPackage->SetPackageFlags(PKG_CompiledIn | 0x00000000);
			FGuid Guid;
			Guid.A = 0xD69C4778;
			Guid.B = 0xF6352CC6;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
