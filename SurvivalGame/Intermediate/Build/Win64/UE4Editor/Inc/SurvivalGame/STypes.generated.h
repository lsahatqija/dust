// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_STypes_generated_h
#error "STypes.generated.h already included, missing '#pragma once' in STypes.h"
#endif
#define SURVIVALGAME_STypes_generated_h

#define SurvivalGame_Source_SurvivalGame_STypes_h_35_GENERATED_BODY \
	friend SURVIVALGAME_API class UScriptStruct* Z_Construct_UScriptStruct_FTakeHitInfo(); \
	SURVIVALGAME_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__EnsureReplicationByte() { return STRUCT_OFFSET(FTakeHitInfo, EnsureReplicationByte); } \
	FORCEINLINE static uint32 __PPO__GeneralDamageEvent() { return STRUCT_OFFSET(FTakeHitInfo, GeneralDamageEvent); } \
	FORCEINLINE static uint32 __PPO__PointDamageEvent() { return STRUCT_OFFSET(FTakeHitInfo, PointDamageEvent); } \
	FORCEINLINE static uint32 __PPO__RadialDamageEvent() { return STRUCT_OFFSET(FTakeHitInfo, RadialDamageEvent); }


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_STypes_h


#define FOREACH_ENUM_EBOTBEHAVIORTYPE(op) \
	op(EBotBehaviorType::Passive) \
	op(EBotBehaviorType::Patrolling) 
#define FOREACH_ENUM_EINVENTORYSLOT(op) \
	op(EInventorySlot::Hands) \
	op(EInventorySlot::Primary) \
	op(EInventorySlot::Secondary) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
