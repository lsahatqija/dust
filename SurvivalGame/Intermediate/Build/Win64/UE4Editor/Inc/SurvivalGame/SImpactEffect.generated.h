// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SImpactEffect_generated_h
#error "SImpactEffect.generated.h already included, missing '#pragma once' in SImpactEffect.h"
#endif
#define SURVIVALGAME_SImpactEffect_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASImpactEffect(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASImpactEffect(); \
public: \
	DECLARE_CLASS(ASImpactEffect, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASImpactEffect) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_INCLASS \
private: \
	static void StaticRegisterNativesASImpactEffect(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASImpactEffect(); \
public: \
	DECLARE_CLASS(ASImpactEffect, AActor, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASImpactEffect) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASImpactEffect(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASImpactEffect) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASImpactEffect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASImpactEffect); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASImpactEffect(ASImpactEffect&&); \
	NO_API ASImpactEffect(const ASImpactEffect&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASImpactEffect(ASImpactEffect&&); \
	NO_API ASImpactEffect(const ASImpactEffect&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASImpactEffect); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASImpactEffect); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASImpactEffect)


#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_PRIVATE_PROPERTY_OFFSET
#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_8_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Items_SImpactEffect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
