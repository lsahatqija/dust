// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SURVIVALGAME_SBotWaypoint_generated_h
#error "SBotWaypoint.generated.h already included, missing '#pragma once' in SBotWaypoint.h"
#endif
#define SURVIVALGAME_SBotWaypoint_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_RPC_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASBotWaypoint(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBotWaypoint(); \
public: \
	DECLARE_CLASS(ASBotWaypoint, ATargetPoint, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASBotWaypoint) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASBotWaypoint(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBotWaypoint(); \
public: \
	DECLARE_CLASS(ASBotWaypoint, ATargetPoint, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASBotWaypoint) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASBotWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASBotWaypoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASBotWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASBotWaypoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASBotWaypoint(ASBotWaypoint&&); \
	NO_API ASBotWaypoint(const ASBotWaypoint&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASBotWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASBotWaypoint(ASBotWaypoint&&); \
	NO_API ASBotWaypoint(const ASBotWaypoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASBotWaypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASBotWaypoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASBotWaypoint)


#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET
#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_11_PROLOG
#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_AI_SBotWaypoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
