// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRotator;
#ifdef SURVIVALGAME_SBaseCharacter_generated_h
#error "SBaseCharacter.generated.h already included, missing '#pragma once' in SBaseCharacter.h"
#endif
#define SURVIVALGAME_SBaseCharacter_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_RPC_WRAPPERS \
	virtual bool ServerSetTargeting_Validate(bool ); \
	virtual void ServerSetTargeting_Implementation(bool NewTargeting); \
	virtual bool ServerSetSprinting_Validate(bool ); \
	virtual void ServerSetSprinting_Implementation(bool NewSprinting); \
 \
	DECLARE_FUNCTION(execOnRep_LastTakeHitInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_LastTakeHitInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAimOffsets) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FRotator*)Z_Param__Result=this->GetAimOffsets(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsTargeting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsTargeting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSetTargeting) \
	{ \
		P_GET_UBOOL(Z_Param_NewTargeting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSetTargeting_Validate(Z_Param_NewTargeting)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSetTargeting_Validate")); \
			return; \
		} \
		this->ServerSetTargeting_Implementation(Z_Param_NewTargeting); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSetSprinting) \
	{ \
		P_GET_UBOOL(Z_Param_NewSprinting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSetSprinting_Validate(Z_Param_NewSprinting)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSetSprinting_Validate")); \
			return; \
		} \
		this->ServerSetSprinting_Implementation(Z_Param_NewSprinting); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsSprinting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsSprinting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsAlive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsAlive(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->GetHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMaxHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->GetMaxHealth(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRep_LastTakeHitInfo) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnRep_LastTakeHitInfo(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetAimOffsets) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FRotator*)Z_Param__Result=this->GetAimOffsets(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsTargeting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsTargeting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSetTargeting) \
	{ \
		P_GET_UBOOL(Z_Param_NewTargeting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSetTargeting_Validate(Z_Param_NewTargeting)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSetTargeting_Validate")); \
			return; \
		} \
		this->ServerSetTargeting_Implementation(Z_Param_NewTargeting); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execServerSetSprinting) \
	{ \
		P_GET_UBOOL(Z_Param_NewSprinting); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		if (!this->ServerSetSprinting_Validate(Z_Param_NewSprinting)) \
		{ \
			RPC_ValidateFailed(TEXT("ServerSetSprinting_Validate")); \
			return; \
		} \
		this->ServerSetSprinting_Implementation(Z_Param_NewSprinting); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsSprinting) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsSprinting(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIsAlive) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->IsAlive(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->GetHealth(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetMaxHealth) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=this->GetMaxHealth(); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_EVENT_PARMS \
	struct SBaseCharacter_eventServerSetSprinting_Parms \
	{ \
		bool NewSprinting; \
	}; \
	struct SBaseCharacter_eventServerSetTargeting_Parms \
	{ \
		bool NewTargeting; \
	};


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASBaseCharacter(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBaseCharacter(); \
public: \
	DECLARE_CLASS(ASBaseCharacter, ACharacter, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASBaseCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASBaseCharacter(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASBaseCharacter(); \
public: \
	DECLARE_CLASS(ASBaseCharacter, ACharacter, COMPILED_IN_FLAGS(CLASS_Abstract), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASBaseCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASBaseCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASBaseCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASBaseCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASBaseCharacter(ASBaseCharacter&&); \
	NO_API ASBaseCharacter(const ASBaseCharacter&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASBaseCharacter(ASBaseCharacter&&); \
	NO_API ASBaseCharacter(const ASBaseCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASBaseCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASBaseCharacter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASBaseCharacter)


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SprintingSpeedModifier() { return STRUCT_OFFSET(ASBaseCharacter, SprintingSpeedModifier); } \
	FORCEINLINE static uint32 __PPO__bWantsToRun() { return STRUCT_OFFSET(ASBaseCharacter, bWantsToRun); } \
	FORCEINLINE static uint32 __PPO__bIsTargeting() { return STRUCT_OFFSET(ASBaseCharacter, bIsTargeting); } \
	FORCEINLINE static uint32 __PPO__TargetingSpeedModifier() { return STRUCT_OFFSET(ASBaseCharacter, TargetingSpeedModifier); } \
	FORCEINLINE static uint32 __PPO__Health() { return STRUCT_OFFSET(ASBaseCharacter, Health); } \
	FORCEINLINE static uint32 __PPO__LastTakeHitInfo() { return STRUCT_OFFSET(ASBaseCharacter, LastTakeHitInfo); }


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_9_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_Player_SBaseCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
