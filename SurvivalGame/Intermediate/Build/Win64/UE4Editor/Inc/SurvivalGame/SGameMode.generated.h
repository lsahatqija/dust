// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef SURVIVALGAME_SGameMode_generated_h
#error "SGameMode.generated.h already included, missing '#pragma once' in SGameMode.h"
#endif
#define SURVIVALGAME_SGameMode_generated_h

#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_RPC_WRAPPERS \
	virtual bool CheckRelevance_Implementation(AActor* Other); \
 \
	DECLARE_FUNCTION(execCheckRelevance) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->CheckRelevance_Implementation(Z_Param_Other); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCheckRelevance) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->CheckRelevance_Implementation(Z_Param_Other); \
		P_NATIVE_END; \
	}


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_EVENT_PARMS \
	struct SGameMode_eventCheckRelevance_Parms \
	{ \
		AActor* Other; \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		SGameMode_eventCheckRelevance_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_CALLBACK_WRAPPERS
#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASGameMode(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameMode(); \
public: \
	DECLARE_CLASS(ASGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASGameMode(); \
	friend SURVIVALGAME_API class UClass* Z_Construct_UClass_ASGameMode(); \
public: \
	DECLARE_CLASS(ASGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/SurvivalGame"), NO_API) \
	DECLARE_SERIALIZER(ASGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASGameMode(ASGameMode&&); \
	NO_API ASGameMode(const ASGameMode&); \
public:


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASGameMode(ASGameMode&&); \
	NO_API ASGameMode(const ASGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASGameMode)


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAllowFriendlyFireDamage() { return STRUCT_OFFSET(ASGameMode, bAllowFriendlyFireDamage); } \
	FORCEINLINE static uint32 __PPO__bSpawnZombiesAtNight() { return STRUCT_OFFSET(ASGameMode, bSpawnZombiesAtNight); } \
	FORCEINLINE static uint32 __PPO__BotPawnClass() { return STRUCT_OFFSET(ASGameMode, BotPawnClass); } \
	FORCEINLINE static uint32 __PPO__MutatorClasses() { return STRUCT_OFFSET(ASGameMode, MutatorClasses); }


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_12_PROLOG \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_EVENT_PARMS


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_RPC_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_INCLASS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_CALLBACK_WRAPPERS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_INCLASS_NO_PURE_DECLS \
	SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SurvivalGame_Source_SurvivalGame_Public_World_SGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
